<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class OfficeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'admin']);
        $permissionList = array(
            ['name'=>'view contacts'],

            ['name'=>'write blog'],
            ['name'=>'edit blog'],
            ['name'=>'active blog'],
            ['name'=>'delete blog'],

            ['name'=>'write job'],
            ['name'=>'edit job'],
            ['name'=>'active job'],
            ['name'=>'delete job'],
        );
        foreach ($permissionList as $permission)
        {
            Permission::create($permission);
        }
        $admins  = array(
            [ 'name'=> 'PK','email'=> 'pk@edanpower.co.uk','password'=>  bcrypt('edanpower'),],
            [ 'name'=> 'Admin','email'=> 'info@edanpower.co.uk','password'=>  bcrypt('edanpower'),]
        );
        foreach ($admins as $admin) {
            $user = User::create($admin);
            $user->assignRole($role);
        }

    }
}
