function _closePanel(panel_id) {
    /**
     EXAMPLE - LOCAL STORAGE PANEL REMOVE|UNREMOVE

     // SET PANEL HIDDEN
     localStorage.setItem(panel_id, 'closed');

     // SET PANEL VISIBLE
     localStorage.removeItem(panel_id);
     **/
}
// confirm model on delete
$('a[data-model-confirm]').click(function (ev) {
    var formId = $(this).attr('data-form-id');
    // var actionUrl =  $(this).attr('href');
    if (!$('#dataConfirmModal').length) {
        $('body').append('<div class="modal fade" id="dataConfirmModal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title">Please Confirm</h4> </div><div class="modal-body"> </div><div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <a class="btn btn-primary" id="dataConfirmOK">Ok</a> </div></div></div></div>');
    }
    $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-model-confirm'));
    // $(formId).attr('action', actionUrl);
    $('#dataConfirmOK').click(function () {
        event.preventDefault();
        document.getElementById(formId).submit();
    });
    $('#dataConfirmModal').modal({show: true});
    return false;
});
/** Preloader
 **************************************************************** **/
