function init() {
    var imgDefer = document.getElementsByTagName('.logo>img');
    for (var i=0; i<imgDefer.length; i++) {
        if(imgDefer[i].getAttribute('data-src')) {
            imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
        } } }
window.onload = init;
// confirm model on delete
$('a[data-model-confirm]').click(function (ev) {
    var formId = $(this).attr('data-form-id');
    // var actionUrl =  $(this).attr('href');
    if (!$('#dataConfirmModal').length) {
        $('body').append('<div class="modal fade" id="dataConfirmModal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title">Please Confirm</h4> </div><div class="modal-body"> </div><div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <a class="btn btn-primary" id="dataConfirmOK">Ok</a> </div></div></div></div>');
    }
    $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-model-confirm'));
    // $(formId).attr('action', actionUrl);
    $('#dataConfirmOK').click(function () {
        event.preventDefault();
        document.getElementById(formId).submit();
    });
    $('#dataConfirmModal').modal({show: true});
    return false;
});
var popupSize = {
    width: 780,
    height: 550
};

$(document).on('click', '.social-icon', function (e) {
    var verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
        horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

    var popup = window.open($(this).prop('href'), 'social',
        'width=' + popupSize.width + ',height=' + popupSize.height +
        ',left=' + verticalPos + ',top=' + horisontalPos +
        ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

    if (popup) {
        popup.focus();
        e.preventDefault();
    }

});