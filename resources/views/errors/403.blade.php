@extends('layouts.app')

@section('meta')
    <title>403-Access Forbidden - Edan Power </title>
@endsection

@section('content')
    @component('components.page-title',['title'=>'403-Access Forbidden ','links'=>['Access Forbidden '=>'#']])
    @endcomponent
    <section class="padding-xlg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 hidden-xs">
                    <div class="error-404">
                        403
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <h3 class="nomargin">Sorry, <strong>your are not authorized to access this page!</strong></h3>
                    <p class="nomargin-top size-20 font-lato text-muted">If your are authorized to access but unable to access please contact
                        <a href="mailto:{{env('EMAIL')}}">{{env('EMAIL')}}</a> asap.</p>
                    <div class="divider nomargin-bottom">
                        <!-- divider -->
                    </div>
                    <a class="size-16 font-lato" href="{{url('/')}}"><i class="fa fa-chevron-left margin-right-10 size-12"></i> back to {{config('app.name')}} homepage now!</a>
                </div>
            </div>
        </div>
    </section>
@endsection
