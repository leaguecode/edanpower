@extends('errors.app')
@section('content-body')
    <section class="text-center">

        <h1 class="margin-bottom-20 size-30 uppercase">{{config('app.name')}} IS UNDER MAINTENANCE</h1>
        <p class="size-20 font-lato">We'll be back soon &ndash; please, come back in a few moments.</p>
    </section>
    <section class="text-center">
        <div class="container">

            <div class="row">

                <div class="col-md-4 col-sm-4">
                    <i class="fa fa-info-circle fa-5x"></i>
                    <h2 class="size-17 margin-top-6">WHY {{strtoupper(config('app.name'))}} IS DOWN?</h2>
                    <p class="text-muted">We are working hard to migrate the data to a new datacenter. We'll be back
                        online soon as possible, we really apologize for this inconvenience.</p>
                </div>

                <div class="col-md-4 col-sm-4">
                    <i class="fa fa-clock-o fa-5x"></i>
                    <h2 class="size-17 margin-top-6">WHAT IS THE DOWNTIME?</h2>
                    <p class="text-muted">Unfortunately we do not know the exact downtime, but we esitmate less than an
                        hour.</p>
                </div>

                <div class="col-md-4 col-sm-4">
                    <i class="fa fa-comments-o fa-5x"></i>
                    <h2 class="size-17 margin-top-6">SUPPORT</h2>
                    <p class="text-muted">If you have questions, please do not hesitate to write us an email at <a
                                href="mailto:{{env('EMAIL')}}">{{env('EMAIL')}}</a></p>
                </div>

            </div>

        </div>
    </section>
@endsection
