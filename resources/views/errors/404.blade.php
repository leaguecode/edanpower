@extends('layouts.app')

@section('meta')
    <title>Page Not Found - Edan Power </title>
@endsection

@section('content')
    @component('components.page-title',['title'=>'401-Page Not Found ','links'=>['Page Not Found '=>'#']])
    @endcomponent
    <section class="padding-xlg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 hidden-xs">
                    <div class="error-404">
                        404
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <h3 class="nomargin">Sorry, <strong>The page you requested can not be found!</strong></h3>
                    <p class="nomargin-top size-20 font-lato text-muted">Whoops,Looks like this page doesn't exist.</p>
                    <div class="divider nomargin-bottom">
                        <!-- divider -->
                    </div>
                    <a class="size-16 font-lato" href="{{url('/')}}"><i class="fa fa-chevron-left margin-right-10 size-12"></i> back to {{config('app.name')}} homepage now!</a>
                </div>
            </div>
        </div>
    </section>
@endsection
