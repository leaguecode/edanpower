@extends('layouts.app')

@section('meta')
    <title>401-Unauthorized Access - Edan Power </title>
@endsection

@section('content')
    @component('components.page-title',['title'=>'401-Unauthorized Access','links'=>['Unauthorized Access'=>'#']])
    @endcomponent
    <section class="padding-xlg">
        <div class="container">
            <div class="text-center">
                <h1 class="margin-bottom-20 size-30 uppercase">This part of {{config('app.name')}} is Unauthorized.</h1>
                <p class="size-20 font-lato">If your are authorized to access but unable to access please contact Admin
                    ASAP.</p>
                <div class="container">
                    <a href="{{url('/')}}" class="btn btn-primary btn-lg btn-block"><i
                                class="fa fa-chevron-left margin-right-10 size-12"></i> back
                        to {{config('app.name')}} homepage now!</a>
                </div>
            </div>

        </div>
    </section>
@endsection
