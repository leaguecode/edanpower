<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8"/>
    <meta name="Author" content="EdanPower.co.uk"/>
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>

    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <![endif]-->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="google-site-verification" content="_NzGB__vjGCSxjT5kJbRiNWdZ7mDoYe-ZD2Q8OypLE0"/>
    @include('components.favicons')
    @yield('meta')
    @include('components.header-links')
    @yield('style')
</head>

<body class="smoothscroll enable-animation">

<div id="wrapper">
    <div id="header" class="clearfix text-center">

        <!-- Logo -->
        <a class="logo logo-responsive" href="{{url('/')}}">
            <img src="{{asset('assets/images/edanpower/logo.png')}}" alt="Edan Power Logo"
                 style="height: 60px !important; margin: 3px"/>
        </a>
    </div>
   @yield('content-body')
</div>
{{--SCROLL TO TOP--}}
<a href="#" id="toTop"></a>
{{--PRELOADER--}}
<div id="preloader">
    <div class="inner">
        <span class="loader"></span>
    </div>
</div>
@include('components.footer-links')
@include('vendor.sweet.alert')
@yield('script')
@include('components.analytics')
</body>
</html>