@extends('office.layouts.app')
@section('style')
    <link rel="stylesheet" href="{{asset('assets/plugins/summernote/dist/summernote.css')}}">
@endsection
@section('content')
    @component('office.components.page-title',['title'=>'Jobs Edit','links'=>['Jobs'=>'office.jobs.index','Edit'=>'#']])
    @endcomponent
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
                <strong>Edit Jobs Post</strong>
                <span class="pull-right">Last Edit At: {{$job->edited_at != null ? date('d M, Y h:m a', strtotime($job->edited_at)) : '-NaN-'}}</span>
            </div>
            <div class="panel-body">
                <form action="{{route('office.jobs.update',[$job])}}" method="POST" class="parsley"
                      enctype="multipart/form-data">
                    <fieldset>
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="row">
                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                <div class="col-md-12 col-sm-12">
                                    <label for="title">Title *</label>
                                    <input type="text" name="title" value="{!! $job->title !!}"
                                           class="form-control" required>
                                    @if ($errors->has('title'))
                                        <span class="help-block"> <strong>{{ $errors->first('title') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="description">Content</label>
                                    <textarea name="description" id="description" class="summernote form-control"
                                              data-upload-url="{{ route('office.jobs.upload',[$job])}}"
                                              data-height="200">{!! $job->description !!}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="requirements">Requirements <small class="text-red">Please use same STYLE for better design</small></label>
                                    <textarea name="requirements" id="requirements" class="summernote form-control"
                                              data-upload-url="{{ route('office.jobs.upload',[$job])}}" data-custom-options="2"
                                              data-height="100">{!! $job->requirements !!}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="expecting">Expecting <small class="text-red">Please use same STYLE for better design</small></label>
                                    <textarea name="expecting" id="expecting" class="summernote form-control"
                                              data-upload-url="{{ route('office.jobs.upload',[$job])}}" data-custom-options="2"
                                              data-height="100">{!! $job->expecting !!}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="vacancies">Vacancies </label>
                                    <input type="number" name="vacancies" id="vacancies" class="form-control"
                                           value="{{$job->vacancies}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="ends_at">Job Post Ends on </label>
                                    <input type="text" id="ends_at" class="form-control  datepicker" data-format="yyyy-mm-dd" data-lang="en" data-RTL="false" value="{{$job->ends_at}}" >
                                </div>
                            </div>
                        </div>
                        <hr>
                        <p><b>Meta data (Title, description and keywords) will be automatically assigned if they not
                                provided</b></p>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="meta_title">Meta Title</label>
                                    <input type="text" name="meta_title" id="meta_title" class="form-control"
                                           value="{{$job->meta_title}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="meta_description">Meta Description</label>
                                    <input type="text" name="meta_description" id="meta_description"
                                           class="form-control"
                                           value="{{$job->meta_description}}">
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-mini alert-danger margin-bottom-30"><!-- DANGER -->
                            <strong>Please!</strong>Meta Keyword are mandatory .
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="meta_keywords">Meta Keywords *
                                        <small class="text-muted">Separate with comma " , "</small>
                                    </label>
                                    <br>
                                    <input type="text" name="meta_keywords" id="meta_keywords"
                                           class="form-control tags-input" width="100%"
                                           value="{{$job->meta_keywords}}" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-teal" value="Save">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection