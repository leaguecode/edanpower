@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'Jobs Create','links'=>['Jobs'=>'office.jobs.index','Create'=>'#']])
    @endcomponent
    <div id="content" class="padding-20">
        <div class="panel-heading panel-heading-transparent">
            <strong>Create New Job Post</strong>
        </div>
        <div class="panel-body">
            @component('office.components.create-form',['route'=>'office.jobs.store'])
            @endcomponent
    </div>
@endsection