@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'Jobs','links'=>['Job'=>'office.jobs.index','Show All'=>'#']])

        <a href="{{route('office.jobs.create')}}" class="btn btn-primary btn-sm pull-right ">Create New</a>

    @endcomponent
    <div id="content" class="padding-20">
        @component('office.components.table')
            <table class="fooTableInit" data-sorting="false">
                <thead>
                <tr>
                    <th class="foo-cell nopadding">&nbsp;</th>
                    <th>Title</th>
                    <th data-hide="s300" class="">Status</th>
                    <th data-hide="s300" class="">Applied</th>
                    <th data-hide="s300,s600" class="">Ends<br> Date</th>
                    <th data-hide="all" class="">Published<br> Date</th>
                    <th data-hide="all" class="">Created<br> Date</th>
                    <th data-hide="s300" class="">Action<br> Edit</th>
                    <th data-hide="s300,s600" class="">Creator</th>

                </tr>
                </thead>
                <tbody>
                @foreach($jobs as $job)
                    <tr>
                        <td class="foo-cell">&nbsp;</td>
                        <td>
                            <a href="{{route('office.jobs.show',[$job])}}">{!! $job->title !!}</a>
                        </td>
                        <td>
                            @if($job->active)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-default">Inactive</span>
                            @endif

                        </td>
                        <td><a href="{{route('office.jobs.seekers.index',[$job])}}" class="btn btn-default btn-xs inline" title="show applied"> {{count($job->seekers)}}</a></td>
                        <td>
                            {{$job->published_at != null ? date('d M, Y h:m a', strtotime($job->ends_t)) : '-NaN-'}}
                        </td>
                        <td>
                            {{$job->published_at != null ? date('d M, Y h:m a', strtotime($job->published_at)) : '-NaN-'}}
                        </td>
                        <td>
                            {{$job->created_at != null ? date('d M, Y h:m a', strtotime($job->created_at)) : '-NaN-'}}
                        </td>
                        <td>
                            <form action="{{ route('office.jobs.active',[$job]) }}" method="POST">
                                {{ csrf_field() }}
                                @if($job->active)
                                    <input type="hidden" name="active" value="0">
                                    <input type="submit" value="De-activate" class="btn btn-default btn-xs ">
                                @else
                                    <input type="hidden" name="active" value="1">
                                    <input type="submit" value="Activate" class="btn btn-default btn-xs ">
                                @endif
                                <a href="{{ route('office.jobs.edit',[$job]) }}" class="btn btn-default btn-xs inline"><i
                                            class="fa fa-edit white"></i>
                                    Edit </a>
                                <a href="{{ route('office.jobs.destroy',[$job]) }}" class="btn btn-default btn-xs inline"
                                   onclick="event.preventDefault();
                                           document.getElementById('delete-task-{{ $job->id }}').submit();">
                                    <i class="fa fa-times white"></i>Delete
                                </a>
                            </form>



                            <form id="delete-task-{{ $job->id }}"
                                  action="{{ route('office.jobs.destroy',[$job]) }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>


                        </td>
                        <td>{!! $job->user->name !!}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$jobs->links()}}
        @endcomponent
    </div>
@endsection