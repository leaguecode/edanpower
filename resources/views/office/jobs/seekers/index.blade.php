@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'Jobs','links'=>['Job'=>'office.jobs.index','Seekers'=>'#']])
    @endcomponent
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
                <strong>{{$job->title}} - Seekers</strong>
                <span class="pull-right">
                    {{$seekers->links()}}
                </span>
            </div>
        </div>
        <div class="row">
            @foreach($seekers as $seeker)
                <div class="panel panel-default">
                    <div class="col-md-6 col-sm-6">
                        <div class="panel-body">
                            <table class="table table-striped table-hover ">
                                <thead>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Job Title</td>
                                    <td>{{$job->title}}</td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td>{{$seeker->name}}</td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td><a href="tel:{{$seeker->phone}}">{{$seeker->phone}}</a></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><a href="mailto:{{$seeker->email}}">{{$seeker->email}}</a></td>
                                </tr>
                                <tr>
                                    <td>Received Date</td>
                                    <td>{{$seeker->created_at != null ? date('d M, Y h:m a', strtotime($seeker->created_at)) : '-NaN-'}}</td>

                                </tr>
                                <tr>
                                    <td>Message</td>
                                    <td>{!! $seeker->description !!}</td>
                                </tr>

                                <tr>
                                    <td>Attachments</td>
                                    <td>
                                        @foreach($seeker->media as $file)
                                            <a target="_blank" href="{{asset('storage/'.$file->name)}}">LINK</a><br>
                                        @endforeach
                                    </td>
                                </tr>
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
            @endforeach

        </div>
@endsection