@extends('layouts.app')
@section('content')
    @component('components.page-title',['title'=>'Career','links'=>['Career'=>'career',$job->title=>'#']])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <div class="row">

                <div class="col-md-7 col-sm-7">
                    <div class="heading-title heading-border-bottom heading-color">
                        <span class="pull-right text-muted">{{$job->vacancies}} available</span>
                        <h2 class="size-20"><span>0{{$job->id}}.</span> {!! strtoupper($job->title) !!}</h2>
                    </div>
                    <div class="margin-bottom-80">
                        {!! $job->description !!}
                        <h5>REQUIREMENTS</h5>
                        <div class="requirements">
                            {!! $job->requirements !!}
                        </div>
                        <h5>WHAT WE EXPECT?</h5>
                        <div class="expecting">
                            {!!$job->expecting !!}
                        </div>

                        <a href="#careers-apply" class="btn btn-3d btn-teal scrollTo" data-offset="150"><i
                                    class="fa fa-check"></i> APPLY NOW</a>
                    </div>
                </div>

                <div class="col-md-5 col-sm-5">
                    <div class="heading-title heading-border-bottom">
                        <h2 class="size-20">APPLY NOW</h2>
                    </div>
                    <form action="{{route('jobs.seeker',[$job])}}" method="post" class="parsley" id="careers-apply"
                          enctype="multipart/form-data" >
                        <fieldset disabled>
                            <!-- required [php action request] -->
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label>Full Name *</label>
                                        <input type="text" name="name" value="{{ old('name') }}"
                                               class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email">E-mail Address *</label>
                                        <input type="email" value="{{ old('email') }}" class="form-control" name="email"
                                               id="email" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label for="phone">Phone *</label>
                                        <input type="number" value="{{ old('phone') }}" class="form-control"
                                               name="phone"
                                               id="phone" required>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">

                                        <label>Experience *</label>
                                        <textarea name="description" rows="4"
                                                  class="form-control" required>{{old('description')}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>
                                            File Attachment
                                            <small class="text-muted">Curriculum Vitae - optional</small>
                                        </label>
                                        <!-- custom file upload -->
                                        <div class="fancy-file-upload fancy-file-primary">
                                            <i class="fa fa-upload"></i>
                                            <input type="file" class="form-control" name="file" accept="image/jpeg,application/zip,image/png,application/pdf"
                                                   onchange="jQuery(this).next('input').val((this.value).replace(/C:\\fakepath\\/i, ''));"/>
                                            <input type="text" class="form-control" placeholder="no file selected"
                                                   readonly=""/>
                                            <span class="button">Choose File</span>
                                        </div>
                                        <small class="text-muted block">Max file size: 10Mb (pdf/jpg/png)</small>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30">
                                    SEND APPLICATION
                                    <span class="block font-lato">We'll get back to you within 48 hours</span>
                                </button>
                            </div>
                        </div>
                    </form>
                    <hr class="margin-top-60"/>

                </div>
                <div class="col-md-12">
                    <div class="divider divider-dotted">
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $('ul').addClass("list-unstyled list-icons");
        $('.requirements>ul>li').append('<i class="fa fa-check"></i>');
        $('.expecting>ul>li').append('<i class="fa fa-plus-square"></i>');
    </script>
@endsection