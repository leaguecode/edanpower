<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8"/>
    <meta name="Author" content="EdanPower.co.uk"/>
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->


    @yield('meta')
    @include('office.components.header-links')
    @yield('style')
</head>

<body>
    @include('office.components.aside')
<div id="wrapper">
    @include('office.components.header')
    <section id="middle">
        @yield('content')
    </section>


</div>

@include('office.components.footer-links')
@yield('script')
    @include('vendor.sweet.alert')
</body>
</html>