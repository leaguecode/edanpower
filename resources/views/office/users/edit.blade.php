@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'User Edit','links'=>['User'=>'office.users.index','Edit'=>'#']])
    @endcomponent
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
                <strong>Create user</strong>
            </div>
            <div class="panel-body">
                <div class="row">
                    <form class="form-horizontal parsley" method="post" action="{{route('office.users.update',[$user])}}">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        @component('office.components.errors')
                        @endcomponent
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">Full Name *</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="name" name="name" required
                                           value="{{$user->name}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="email">Email *</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="email" name="email" value="{{$user->email}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="designation">Designation</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="designation" name="designation"
                                           value="{{$user->designation}}">
                                </div>
                            </div>
                        </fieldset>
                        <div class="row">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">Save User</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection