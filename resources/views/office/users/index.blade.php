@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'User Edit','links'=>['User'=>'office.users.index','Show All'=>'#']])
        <a href="{{route('office.users.create')}}" class="btn btn-primary btn-sm pull-right ">Create New</a>
    @endcomponent
    <div id="content" class="">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
                <strong>All Users</strong>
                <span class="pull-right">
                    {{$users->links()}}
                </span>
            </div>
        </div>

        @foreach($users as $user)
            <div class="col-md-4 col-lg-3">
                <a href="{{route('office.users.show',[$user])}}">
                    <section class="panel panel-default">
                        <div class="panel-body padding-10">
                            <figure class="margin-bottom-10">
                                <img class="img-responsive"
                                     src="{{asset($user->avatar ? 'storage/'.$user->avatar : 'assets/images/avatar2.jpg')}}"
                                     alt="{{$user->name }} Profile Image"/>
                            </figure>

                            <hr class="half-margins"/>
                            <!-- About -->
                            <h3 class="text-black">
                                {{$user->name }}
                                @if($user->designation)
                                    <small class="text-gray size-14"> / {{$user->designation}}</small>
                                @endif
                            </h3>
                            <p class="size-12">{!! $user->about !!}</p>
                            <p><a href="tel:{{$user->phone}}">{{$user->phone}}</a></p>
                            <p><a href="mailto:{{$user->email}}">{{$user->email}}</a></p>

                        </div>
                    </section>
                </a>
            </div>
        @endforeach

    </div>
@endsection