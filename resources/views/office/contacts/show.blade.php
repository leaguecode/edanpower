@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'Contact','links'=>['Contacts'=>'office.contacts.index','view'=>'#']])
    @endcomponent
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
                <strong>Full contact</strong>
            </div>
            <div class="panel-body">

                    <table class="table table-striped table-hover ">
                        <thead>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Site</td>
                            <td>{{$contact->site}}</td>
                        </tr>
                        <tr>
                            <td>Company</td>
                            <td>{{$contact->company}}</td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>{{$contact->name}}</td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><a href="tel:{{$contact->phone}}">{{$contact->phone}}</a></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><a href="mailto:{{$contact->email}}">{{$contact->email}}</a></td>
                        </tr>
                        <tr>
                            <td>Message</td>
                            <td>{!! $contact->message !!}</td>
                        </tr>
                        <tr>
                            <td>Other</td>
                            <td>{!! $contact->other !!}</td>
                        </tr>
                        <tr>
                            <td>IP</td>
                            <td>{{$contact->ip}}</td>
                        </tr>
                        <tr>
                            <td>Received Date</td>
                            <td>{{$contact->created_at != null ? date('d M, Y h:m a', strtotime($contact->created_at)) : '-NaN-'}}</td>
                        </tr>
                        <tr>
                            <td>Attachments</td>
                            <td>
                                @foreach($contact->media as $file)

                                    <a target="_blank" href="{{asset('storage/'.$file->name)}}">LINK</a><br>
                                @endforeach
                            </td>
                        </tr>


                        </tbody>
                    </table>



            </div>
        </div>

    </div>
@endsection