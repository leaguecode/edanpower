@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'Contacts ' ,'links'=>['Contact'=>'office.jobs.index','Show All'=>'#']])
    @endcomponent
    <div id="content" class="padding-20">
        @component('office.components.table')
            <table class="fooTableInit" data-sorting="false">
                <thead>
                <tr>
                    <th class="foo-cell nopadding">&nbsp;Site</th>
                    <th>Company</th>
                    <th data-hide="s300" class="">Name</th>
                    <th data-hide="s300,s600" class="">Email</th>
                    <th data-hide="s300,s600" class="">Phone</th>
                    <th data-hide="s300" class="">Action</th>
                    <th data-hide="all" class="">IP</th>
                    <th data-hide="all" class="">Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($contacts as $contact)
                    <tr>
                        <td class="foo-cell">&nbsp;{{$contact->site}}</td>
                        <td>{!! $contact->company !!}
                        </td>
                        <td>{!! $contact->name !!}</td>
                        <td>{!! $contact->email !!}</td>
                        <td>{!! $contact->phone !!}</td>
                        <td> <a href="{{ route('office.contacts.show',[$contact]) }}" class="btn btn-default btn-xs inline"><i
                                        class="fa fa-edit white"></i>
                                Open </a></td>
                        <td>{!! $contact->ip !!}</td>
                        <td>
                            {{$contact->created_at != null ? date('d M, Y h:m a', strtotime($contact->created_at)) : '-NaN-'}}
                        </td>


                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$contacts->links()}}
        @endcomponent
    </div>
@endsection