@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'Blog Edit','links'=>['Blog'=>'office.blog.index','Preview'=>'#']])
    @endcomponent
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
                <strong>Blog Preview <small class="text-muted">This is only sample view for reading purpose</small></strong>
            </div>
            <div class="panel-body padding-0">
                <iframe  src="{{route('office.blog.preview',[$blog])}}" style="height: 100vh; width: 100%;"></iframe>
            </div>
        </div>

    </div>
@endsection