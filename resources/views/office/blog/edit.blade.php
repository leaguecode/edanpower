@extends('office.layouts.app')
@section('style')
    <link rel="stylesheet" href="{{asset('assets/plugins/summernote/dist/summernote.css')}}">
    @endsection
@section('content')
    @component('office.components.page-title',['title'=>'Blog Edit','links'=>['Blog'=>'office.blog.index','Edit'=>'#']])
    @endcomponent
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
                <strong>Edit Blog Post</strong>
                <span class="pull-right">Last Edit At: {{$blog->edited_at != null ? date('d M, Y h:m a', strtotime($blog->edited_at)) : '-NaN-'}}</span>
            </div>
            <div class="panel-body">
                <form action="{{route('office.blog.update',[$blog])}}" method="POST" class="parsley"
                      enctype="multipart/form-data">
                    <fieldset>
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="row">
                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                <div class="col-md-12 col-sm-12">
                                    <label for="title">Title *</label>
                                    <input type="text" name="title" value="{!! $blog->title !!}"
                                           class="form-control" required>
                                    @if ($errors->has('title'))
                                        <span class="help-block"> <strong>{{ $errors->first('title') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="description">Content</label>
                                    <textarea name="description" id="description" class="summernote form-control"
                                              data-upload-url="{{ route('office.blog.upload',[$blog])}}"
                                              data-height="200">{!! $blog->description !!}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="keywords">Keywords
                                        <small class="text-muted">Separate with comma " , "</small>
                                    </label>
                                    <br>
                                    <input type="text" name="keywords" id="keywords" class="form-control tags-input"
                                           value="{{$blog->keywords}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label>
                                        Cover Image  <small><a target="_blank" href="{{asset('storage/'.$blog->cover)}}"> {{$blog->cover}}</a></small>
                                    </label>
                                    <input class="custom-file-upload" name="file" type="file" id="file"
                                           data-btn-text="Select a Image"/>
                                    <small class="text-muted block">jpg/png</small>
                                </div>
                                <div class="col-md-6">
                                    <div class="thumbnail">
                                        <img src="{{asset('storage/'.$blog->cover)}}" alt="" class="img-responsive">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <hr>
                        <p><b>Meta data (Title, description and keywords) will be automatically assigned if they not
                                provided</b></p>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="meta_title">Meta Title</label>
                                    <input type="text" name="meta_title" id="meta_title" class="form-control"
                                           value="{{$blog->meta_title}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="meta_description">Meta Description</label>
                                    <input type="text" name="meta_description" id="meta_description"
                                           class="form-control"
                                           value="{{$blog->meta_description}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label for="meta_keywords">Meta Keywords
                                        <small class="text-muted">Separate with comma " , "</small>
                                    </label>
                                    <br>
                                    <input type="text" name="meta_keywords" id="meta_keywords"
                                           class="form-control tags-input" width="100%"
                                           value="{{$blog->meta_keywords}}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-teal" value="Save">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection