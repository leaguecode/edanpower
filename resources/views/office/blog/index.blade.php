@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'Blog','links'=>['Blog'=>'office.blog.index','Show All'=>'#']])

        <a href="{{route('office.blog.create')}}" class="btn btn-primary btn-sm pull-right ">Create New</a>

    @endcomponent
    <div id="content" class="padding-20">
        @component('office.components.table')
            <table class="fooTableInit" data-sorting="false">
                <thead>
                <tr>
                    <th class="foo-cell nopadding">&nbsp;</th>
                    <th>Title</th>
                    <th data-hide="s300" class="">Status</th>
                    <th data-hide="s300,s600" class="">Published<br> Date</th>
                    <th data-hide="s300,s600" class="">Created<br> Date</th>
                    <th data-hide="s300" class="">Action<br> Edit</th>
                    <th data-hide="s300,s600" class="">Creator</th>

                </tr>
                </thead>
                <tbody>
                @foreach($blogs as $blog)
                    <tr>
                        <td class="foo-cell">&nbsp;</td>
                        <td><a href="{{route('office.blog.show',[$blog])}}">{!! $blog->title !!}</a>
                        </td>
                        <td>
                            @if($blog->active)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-default">Inactive</span>
                            @endif

                        </td>
                        <td>
                            {{$blog->published_at != null ? date('d M, Y h:m a', strtotime($blog->published_at)) : '-NaN-'}}
                        </td>
                        <td>
                            {{$blog->created_at != null ? date('d M, Y h:m a', strtotime($blog->created_at)) : '-NaN-'}}
                        </td>
                        <td>
                            <form action="{{ route('office.blog.active',[$blog]) }}" method="POST">
                                {{ csrf_field() }}
                                @if($blog->active)
                                    <input type="hidden" name="active" value="0">
                                    <input type="submit" value="De-activate" class="btn btn-default btn-xs ">
                                @else
                                    <input type="hidden" name="active" value="1">
                                    <input type="submit" value="Activate" class="btn btn-default btn-xs ">
                                @endif
                                <a href="{{ route('office.blog.edit',[$blog]) }}" class="btn btn-default btn-xs inline"><i
                                            class="fa fa-edit white"></i>
                                    Edit </a>
                                <a href="{{ route('office.blog.destroy',[$blog]) }}" class="btn btn-default btn-xs inline"
                                   onclick="event.preventDefault();
                                           document.getElementById('delete-task-{{ $blog->id }}').submit();">
                                    <i class="fa fa-times white"></i>Delete
                                </a>
                            </form>



                            <form id="delete-task-{{ $blog->id }}"
                                  action="{{ route('office.blog.destroy',[$blog]) }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>


                        </td>
                        <td>{!! $blog->user->name !!}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$blogs->links()}}
        @endcomponent
    </div>
@endsection