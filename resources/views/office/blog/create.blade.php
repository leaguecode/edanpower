@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'Blog Create','links'=>['Blog'=>'office.blog.index','Create'=>'#']])
    @endcomponent
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-transparent">
                <strong>Create new Blog Post</strong>
            </div>
            <div class="panel-body">
                @component('office.components.create-form',['route'=>'office.blog.store'])
                @endcomponent

            </div>
        </div>
    </div>
@endsection