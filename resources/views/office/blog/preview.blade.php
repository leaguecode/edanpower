@extends('layouts.app')
@section('content')
    @component('components.page-title',['title'=>'Blog','links'=>['Blog'=>'blog',$blog->title=>'#']])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <h1 class="blog-post-title">{!! $blog->title !!}</h1>
            <ul class="blog-post-info list-inline">
                <li>
                    <a href="#">
                        <i class="fa fa-clock-o"></i>
                        <span class="font-lato">{{date('d M, Y',strtotime($blog->published_at))}}</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span class="font-lato">{{$blog->user->name}}</span>
                    </a>
                </li>
            </ul>
            <div class="blog-single-small-media inverse">
                <figure>
                    <img class="img-responsive" src="{{asset('storage/'.$blog->cover)}}" alt="Edan Power{{$blog->title}}" />
                </figure>
            </div>
            <div class="blog-body">
                {!!  $blog->description !!}
            </div>

            <br>
            <br>
            <br>
            <div class="divider divider-dotted">
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script>
        $('.blog-body').first().addClass("dropcap")
    </script>
@endsection
