@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'User Profile','links'=>['Settings'=>'#']])
    @endcomponent
    <div id="content" class="padding-20">
        <div class="page-profile">
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <section class="panel">
                        <div class="panel-body noradius padding-10">
                            <figure class="margin-bottom-10">
                                <img class="img-responsive"
                                     src="{{asset(Auth::user()->avatar ? 'storage/'.Auth::user()->avatar : 'assets/images/avatar2.jpg')}}"
                                     alt="{{Auth::user()->name }} Profile Image"/>
                            </figure>
                            <hr class="half-margins"/>
                            <h3 class="text-black">
                                {{Auth::user()->name }}
                                @if(Auth::user()->designation)
                                    <small class="text-gray size-14"> / {{Auth::user()->designation}}</small>
                                @endif
                            </h3>
                            <p class="size-12">{!! Auth::user()->about !!}</p>


                        </div>
                    </section>
                </div>
                <div class="col-md-8 col-lg-8">
                    <div class="tabs white nomargin-top">
                        <ul class="nav nav-tabs tabs-primary">
                            <li class="{{Route::currentRouteName() == 'office.settings.profile.index' ? 'active' :''}}">
                                <a href="{{route('office.settings.profile.index')}}">Profile</a>
                            </li>
                            <li class="{{Route::currentRouteName() == 'office.settings.avatar.index' ? 'active' :''}}">
                                <a href="{{route('office.settings.avatar.index')}}">Avatar</a>
                            </li>
                            <li class="{{Route::currentRouteName() == 'office.settings.password.index' ? 'active' :''}}">
                                <a href="{{route('office.settings.password.index')}}">Password</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            @yield('tab')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection