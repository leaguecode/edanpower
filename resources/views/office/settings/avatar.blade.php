@extends('office.settings.index')
@section('tab')
    <div id="avatar" class="tab-pane active">
        <form class="clearfix parsley" action="{{route('office.settings.avatar.index')}}" method="post" name="avatar"
              enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="thumbnail">

                            <img class="img-responsive" id="profileImage"
                                 src="{{asset(Auth::user()->avatar ? 'storage/'.Auth::user()->avatar : 'assets/images/avatar2.jpg')}}"
                                 alt="{{Auth::user()->name }} Profile Image"/>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8">
                        <div class="sky-form nomargin form-group">
                            <label class="label" for="file">Select File</label>
                            <input class="custom-file-upload" type="file" id="file"
                                   onchange="document.getElementById('profileImage').src = window.URL.createObjectURL(this.files[0])"
                                   name="file" data-btn-text="Select a File" required/>

                        </div>
                        @if(Auth::user()->avatar)
                            <a href="{{route('office.settings.avatar.delete',['name'=>Auth::user()->avatar])}}"
                               class="btn btn-danger btn-xs noradius"><i class="fa fa-times"></i> Remove Avatar</a>
                        @endif
                        <div class="clearfix margin-top-20">
                            @component('office.components.errors')
                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
            <div class="margiv-top10">
                <button type="submit" class="btn btn-primary">Save Changes</button>

            </div>
        </form>
    </div>
@endsection