@extends('office.settings.index')
@section('tab')
    <div id="profile" class="tab-pane active">
        <form class="form-horizontal parsley" method="post" action="{{route('office.settings.profile.update')}}">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <h4>Personal Information</h4>
            @component('office.components.errors')
            @endcomponent
            <fieldset>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="name">Full Name*</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="name" name="name" required value="{{Auth::user()->name}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="email">Email</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="email" value="{{Auth::user()->email}}" readonly="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="about">Biographical Info</label>
                    <div class="col-md-8">
                        <textarea class="form-control" rows="3" id="about" name="about">{!! Auth::user()->about !!}</textarea>
                    </div>
                </div>

            </fieldset>
            <hr/>
            <h4>Privacy Info
                <small>This is only visiable to Adminster only</small>
            </h4>
            <fieldset>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="address">Address</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="address" name="address" onFocus="geolocate()"  value="{{Auth::user()->address}}">
                    </div>
                </div>
                <div class="form-group ">
                    <label class="col-md-3 control-label" for="phone">Phone</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control masked" data-format="+44 9999 999999" data-placeholder="X" name="phone" placeholder="Phone" value="{{Auth::user()->phone}}">
                    </div>
                </div>
            </fieldset>
            <div class="row">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhxvjd0tdMdjID3F-SoJVsqLmnhn7UWrU&libraries=places&callback=initAutocomplete"
            async defer></script>
    <script>
        $(document).keypress(
            function(event){
                if (event.which == '13') {
                    event.preventDefault();
                }
            });
        var placeSearch, autocomplete;
        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('address')),
                {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
//            autocomplete.addListener('place_changed', fillInAddress);
        }
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }
    </script>
    @endsection