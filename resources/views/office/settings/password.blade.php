@extends('office.settings.index')
@section('tab')
    <div id="password" class="tab-pane active">
        <form class="form-horizontal parsley" method="post" action="{{route('office.settings.password.update')}}">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <h4>Change Password</h4>
            @component('office.components.errors')
            @endcomponent
            <fieldset class="mb-xl">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="newPassword">New Password</label>
                    <div class="col-md-8">
                        <input type="password" class="form-control" id="newPassword" name="password" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="newPasswordRepeat">Repeat New Password</label>
                    <div class="col-md-8">
                        <input type="password" class="form-control" id="newPasswordRepeat" name="password_confirmation" required data-parsley-equalto="#newPassword">
                    </div>
                </div>
            </fieldset>
            <div class="row">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">Change Password</button>
                </div>
            </div>
        </form>
    </div>
@endsection