<aside id="aside">
    <nav id="sideNav">
        <ul class="nav nav-list">
            <li>
                <a class="dashboard" href="{{route('office.dashboard.index')}}">
                    <i class="main-icon fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-inbox"></i> <span>Contacts</span>
                </a>
                <ul>
                    <li><a href="{{route('office.contacts.index')}}">View All</a></li>
                    <li><a href="{{route('office.contacts.site',['site'=>'edanpower'])}}">Edan Power</a></li>
                    <li><a href="{{route('office.contacts.site',['site'=>'edancover'])}}">Edan Cover</a></li>
                    <li><a href="{{route('office.contacts.site',['site'=>'edantalk'])}}">Edan Talk</a></li>
                    <li><a href="{{route('office.contacts.site',['site'=>'edanpay'])}}">Edan Pay</a></li>
                    <li><a href="{{route('office.contacts.site',['site'=>'cardmachines'])}}">Card Machines</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-rss"></i> <span>Blog</span>
                </a>
                <ul>
                    <li><a href="{{route('office.blog.index')}}">View All</a></li>
                    <li><a href="{{route('office.blog.create')}}">Create</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-graduation-cap"></i> <span>Jobs</span>
                </a>
                <ul>
                    <li><a href="{{route('office.jobs.index')}}">View All</a></li>
                    <li><a href="{{route('office.jobs.create')}}">Create</a></li>
                </ul>
            </li>

        </ul>

        <h3>MORE</h3>
        <ul class="nav nav-list">
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-users"></i> <span>Users</span>
                </a>
                <ul>
                    <li><a href="{{route('office.users.index')}}">View All</a></li>
                    <li><a href="{{route('office.users.create')}}">Create</a></li>
                </ul>

            </li>
            {{--<li>--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-menu-arrow pull-right"></i>--}}
                    {{--<i class="main-icon fa fa-link"></i> <span>Permissions & Roles</span>--}}
                {{--</a>--}}
                {{--<ul>--}}
                   {{--<li>--}}
                       {{--<a href="#">--}}
                           {{--<i class="fa fa-menu-arrow pull-right"></i>Permissions--}}
                       {{--</a>--}}
                       {{--<ul>--}}
                           {{--<li><a href="{{route('office.permissions.index')}}">View All</a></li>--}}
                           {{--<li><a href="{{route('office.permissions.create')}}">Create</a></li>--}}
                       {{--</ul>--}}
                   {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="#">--}}
                            {{--<i class="fa fa-menu-arrow pull-right"></i>Roles--}}
                        {{--</a>--}}
                        {{--<ul>--}}
                            {{--<li><a href="{{route('office.roles.index')}}">View All</a></li>--}}
                            {{--<li><a href="{{route('office.roles.create')}}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul>
    </nav>
    <span id="asidebg">
    </span>
</aside>