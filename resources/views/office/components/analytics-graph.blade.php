<canvas class="chartjs fullwidth height-300" id="lineChartCanvas" width="547" height="300"></canvas>
@section('script')
    <script type="text/javascript">
        loadScript(plugin_path + 'chart.chartjs/Chart.min.js', function () {
            var d = @json($data);
            var dates = [];
            var visitors = [];
            var pageViews = [];
            $.each(d, function (index, value) {
                dates.push(value[0]);
                visitors.push(value[1]);
                pageViews.push(value[2])
            });
            var lineChartCanvas = {
                labels: dates,
                datasets: [
                    {
                        label: "Visitors",
                        fillColor: "rgba(220,220,220,0.2)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: visitors
                    },
                    {
                        label: "Page Views",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: pageViews
                    }
                ]
            };

            var ctx = document.getElementById("lineChartCanvas").getContext("2d");
            new Chart(ctx).Line(lineChartCanvas);
        });
    </script>
@endsection