<form action="{{route($route)}}" method="POST" class="parsley">
    <fieldset>
        {{csrf_field()}}
        <div class="row">
            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                <div class="col-md-12 col-sm-12">
                    <label for="title">Title *</label>
                    <input type="text" name="title" value="{{ old('title') }}"
                           class="form-control" required>
                    @if ($errors->has('title'))
                        <span class="help-block">
                     <strong>{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>
                {{$slot}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" class="btn btn-teal" value="Save">
            </div>
        </div>
    </fieldset>
</form>
