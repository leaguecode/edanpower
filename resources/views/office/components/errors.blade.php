@if (count($errors) > 0)
    <div class="alert alert-mini alert-danger margin-bottom-30">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
