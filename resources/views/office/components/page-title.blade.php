
<header id="page-header">
    {{$slot}}
    <h1>{{$title}}</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('office.dashboard.index') }}">Dashboard</a></li>
        @if(isset($links))
            @foreach($links as $key => $link)
                @if($loop->last)
                    <li class="active" >{{$key}}</li>
                @else
                    <li ><a href="{{route($link)}}">{{$key}}</a></li>
                @endif

            @endforeach
        @endif
    </ol>

</header>
