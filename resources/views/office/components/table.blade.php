@section('style')
    <link href="{{asset('assets/plugins/footable/css/footable.core.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/footable/css/footable.standalone.css')}}" rel="stylesheet" type="text/css" />
@endsection

{{ $slot }}

@section('script')
    <script type="text/javascript">
        loadScript(plugin_path + "footable/dist/footable.min.js", function(){
                var $ftable = jQuery('.fooTableInit');

                    /** 01. FOOTABLE INIT
                     ******************************************* **/
                    $ftable.footable({
                        breakpoints: {
                            s300: 300,
                            s600: 600
                        },
                    });
                    /** 01. PER PAGE SWITCH
                     ******************************************* **/
                    jQuery('#change-page-size').change(function (e) {
                        e.preventDefault();
                        var pageSize = jQuery(this).val();
                        $ftable.data('page-size', pageSize);
                        $ftable.trigger('footable_initialized');
                    });

                    jQuery('#change-nav-size').change(function (e) {
                        e.preventDefault();
                        var navSize = jQuery(this).val();
                        $ftable.data('limit-navigation', navSize);
                        $ftable.trigger('footable_initialized');
                    });
        });
    </script>
@endsection