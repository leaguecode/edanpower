<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />
<!-- CORE CSS -->
<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<!-- THEME CSS -->
<link href="{{asset('assets/css/essentials.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/layout.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/color_scheme/green.css')}}" rel="stylesheet" type="text/css" id="color_scheme" />
<link href="{{asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css" id="color_scheme" />