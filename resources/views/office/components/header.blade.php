<header id="header">

    <button id="mobileMenuBtn"></button>
    <span class="logo pull-left">
					<img src="{{asset('assets/images/edanpower/logo.jpg')}}" alt="admin panel" height="35"/>
				</span>
    <nav>
        <ul class="nav pull-right">
            <li class="dropdown pull-left">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                   data-close-others="true">
                    <img class="user-avatar" alt="" src="{{asset(Auth::user()->avatar ? 'storage/'.Auth::user()->avatar : 'assets/images/noavatar.jpg')}}" height="34"/>
                    <span class="user-name">
									<span class="hidden-xs">
										  {{ Auth::user()->name }} <i class="fa fa-angle-down"></i>
									</span>
								</span>
                </a>
                <ul class="dropdown-menu hold-on-click">
                    <li>
                        <a href="{{route('office.settings.profile.index')}}"><i class="fa fa-cogs"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{ route('office.logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                        ><i
                                    class="fa fa-power-off"></i> Log Out</a>
                        <form id="logout-form" action="{{ route('office.logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</header>