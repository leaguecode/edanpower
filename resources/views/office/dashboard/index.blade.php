@extends('office.layouts.app')
@section('content')
    @component('office.components.page-title',['title'=>'Dashboard','links'=>['Dashboard'=>'office.dashboard.index']])
    @endcomponent
    <div id="content" class="padding-20">
        <div id="panel-1" class="panel panel-default">
            <div class="panel-heading">
                        <span class="title elipsis">
								<strong>SITE SUMMARY</strong>
								<small class="size-12 weight-300 text-mutted hidden-xs">Last 15 Days {{date('M Y')}}</small>
							</span>
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse"
                           data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen"
                           data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                </ul>

            </div>

            <div class="panel-body">
                @component('office.components.analytics-graph',['data'=>$data])
                @endcomponent

            </div>
        </div>
    </div>
@endsection