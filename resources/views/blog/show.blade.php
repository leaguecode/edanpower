@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
            {!! $blog->meta_title !!}
        @endslot
        @slot('description')
            {!! $blog->meta_description !!}
        @endslot
        @slot('keywords')
            {!! $blog->meta_keywords !!}
        @endslot
        @slot('image')
            {{asset('storage/'.$blog->cover)}}
        @endslot
    @endcomponent
@endsection
@section('content')
    @component('components.page-title',['title'=>'Blog','links'=>['Blog'=>'blog',$blog->title=>'#']])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <h1 class="blog-post-title">{!! $blog->title !!}</h1>
            <ul class="blog-post-info list-inline">
                <li>
                    <a href="#">
                        <i class="fa fa-clock-o"></i>
                        <span class="font-lato">{{date('d M, Y',strtotime($blog->published_at))}}</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span class="font-lato">{{$blog->user->name}}</span>
                    </a>
                </li>
            </ul>
            <div class="blog-single-small-media inverse">
                <figure>
                    <img class="img-responsive" src="{{asset('storage/'.$blog->cover)}}" alt="Edan Power{{$blog->title}}" />
                </figure>
            </div>
            <div class="blog-body">
                {!!  $blog->description !!}
            </div>

            <br>
            <br>
            <br>
            <div class="divider divider-dotted">

            </div>
            <div class="clearfix margin-top-30">
                    <span class="pull-left margin-top-6 bold hidden-xs">
							Share Post:
						</span>
                <a href="{{Share::load(Request::url(),$blog->title,asset('storage/'.$blog->cover))->facebook()}}" class="social-icon social-icon-sm social-icon-transparent social-facebook pull-right"
                   data-toggle="tooltip" data-placement="top" title="Facebook">
                    <i class="icon-facebook"></i>
                    <i class="icon-facebook"></i>
                </a>
                <a href="{{Share::load(Request::url(),$blog->title,asset('storage/'.$blog->cover))->twitter()}}" class="social-icon social-icon-sm social-icon-transparent social-twitter pull-right"
                   data-toggle="tooltip" data-placement="top" title="Twitter">
                    <i class="icon-twitter"></i>
                    <i class="icon-twitter"></i>
                </a>
                <a href="{{Share::load(Request::url(),$blog->title,asset('storage/'.$blog->cover))->gplus()}}" class="social-icon social-icon-sm social-icon-transparent social-gplus pull-right"
                   data-toggle="tooltip" data-placement="top" title="Google plus">
                    <i class="icon-gplus"></i>
                    <i class="icon-gplus"></i>
                </a>
                <a href="{{Share::load(Request::url(),$blog->title,asset('storage/'.$blog->cover))->linkedin()}}" class="social-icon social-icon-sm social-icon-transparent social-linkedin pull-right"
                   data-toggle="tooltip" data-placement="top" title="Linkedin">
                    <i class="icon-linkedin"></i>
                    <i class="icon-linkedin"></i>
                </a>
                <a href="{{Share::load(Request::url(),$blog->title,asset('storage/'.$blog->cover))->pinterest()}}" class="social-icon social-icon-sm social-icon-transparent social-pinterest pull-right"
                   data-toggle="tooltip" data-placement="top" title="Pinterest">
                    <i class="icon-pinterest"></i>
                    <i class="icon-pinterest"></i>
                </a>
                <a href="{{Share::load(Request::url(),$blog->title,asset('storage/'.$blog->cover))->email()}}" class="social-icon social-icon-sm social-icon-transparent social-call pull-right"
                   data-toggle="tooltip" data-placement="top" title="Email">
                    <i class="icon-email3"></i>
                    <i class="icon-email3"></i>
                </a>
            </div>
            <!-- /SHARE POST -->
        </div>
    </section>

@endsection
@section('script')
    <script>
        $('.blog-body').first().addClass("dropcap")
    </script>
    @endsection
