@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
            Blog
        @endslot
    @endcomponent
@endsection
@section('content')
    @component('components.page-title',['title'=>'Blog','links'=>['Blog'=>'blog']])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <div class="row">

                @foreach($blogs as $blog)
                    <div class="col-md-12">
                        <div class="blog-post-item blog-post-item-inverse">
                            <figure class="blog-item-small-image margin-bottom-20">
                                <img class="img-responsive" src="{{asset('storage/'.$blog->cover)}}"
                                     alt="Edan Power{{$blog->title}}">
                            </figure>
                            <div class="blog-item-small-content">
                                <h2>
                                    <a href="{{route('blog.show',['slug'=>str_slug($blog->title),'blog'=>$blog])}}">{!! $blog->title !!}</a>
                                </h2>
                                <ul class="blog-post-info list-inline">
                                    <li>
                                        <a>
                                            <i class="fa fa-clock-o"></i>
                                            <span class="font-lato">{{date('d M, Y',strtotime($blog->published_at))}}</span>
                                        </a>
                                    </li>
                                </ul>
                                {!! str_limit($blog->description,$limit=350) !!}
                                <br>
                                <br>
                                <a href="{{route('blog.show',['slug'=>str_slug($blog->title),'blog'=>$blog])}}"
                                   class="btn btn-reveal btn-default">
                                    <i class="fa fa-plus"></i>
                                    <span>Read More</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
                    <div class="col-md-6">
                        {{$blogs->links()}}
                    </div>
            </div>
        </div>
    </section>
@endsection
