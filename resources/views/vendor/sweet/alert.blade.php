<link rel="stylesheet" href="{{asset('assets/plugins/sweetalert/dist/sweetalert.css')}}">
<script src="{{asset('assets/plugins/sweetalert/dist/sweetalert.min.js')}}"></script>
@if (Session::has('sweet_alert.alert'))
    <script>
        swal({!! Session::pull('sweet_alert.alert') !!});
    </script>
@endif
