@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
            Career
        @endslot
    @endcomponent
@endsection
@section('content')
    @component('components.page-title',['title'=>'Career','links'=>['Career'=>'career']])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        @foreach($jobs as $job)
                            <div class="col-md-12">
                                <div class="heading-title heading-border-bottom heading-color">
                                    <span class="pull-right text-muted">{{$job->vacancies}} available</span>
                                    <h2 class="size-20"><span>0{{$job->id}}.</span> {!! strtoupper($job->title) !!}</h2>
                                </div>
                                <div class="margin-bottom-50">
                                    {!! $job->description !!}
                                </div>
                                <a href="{{route('jobs.show',['slug'=>str_slug($job->title),'job'=>$job])}}"
                                   class="btn btn-reveal btn-default">
                                    <i class="fa fa-plus"></i>
                                    <span>Read &amp; Apply</span>
                                </a>
                            </div>
                        @endforeach
                        <div class="col-md-6">
                            {{$jobs->links()}}
                        </div>
                        <div class="col-md-12 col-md-12">
                            <hr/>
                            <div class="text-center">
                                <i class="fa fa-envelope fa-3x"></i>
                                <h1 class="font-raleway nomargin"><a href="mailto:{{env('EMAIL')}}">{{env('EMAIL')}}</a>
                                </h1>
                                <span class="size-13 text-muted">FEEL FREE TO E-MAIL US</span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection