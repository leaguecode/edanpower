@extends('office.layouts.auth')

@section('content')
    <div class="login-box">
        @if (session('status'))
            <div class="alert alert-success noradius">
                {{ session('status') }}
            </div>
        @endif @if ($errors->any())
            <div class="alert alert-mini alert-danger mb-30">
                <ul> @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('office.password.request') }}" class="sky-form boxed">
            <header><i class="fa fa-users"></i> Reset Password</header>
            <fieldset>
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <label class="input">
                    <i class="icon-append fa fa-envelope"></i>
                    <input id="email" type="email" class="form-control" name="email"
                           value="{{ $email or old('email') }}" required autofocus placeholder="Email address">

                </label>
                <label class="input">
                    <i class="icon-append fa fa-lock"></i>
                    <input id="password" type="password" class="form-control" name="password" required
                           placeholder="Password">
                    <b class="tooltip tooltip-bottom-right">Only latin characters and numbers</b>
                </label>
                <label class="input">
                    <i class="icon-append fa fa-lock"></i>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                           required placeholder="Confirm password">
                    <b class="tooltip tooltip-bottom-right">Only latin characters and numbers</b>
                </label>
            </fieldset>
            <footer>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Reset Password
                </button>
            </footer>
        </form>
    </div>
@endsection
