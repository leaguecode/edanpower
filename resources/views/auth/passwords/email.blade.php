@extends('office.layouts.auth')

@section('content')
    <div class="login-box">
        @if (session('status'))
            <div class="alert alert-success noradius">
                {{ session('status') }}
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-mini alert-danger mb-30">
                <ul> @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach </ul>
            </div>
        @endif
        <form action="{{ route('office.password.email') }}" method="post" class="sky-form boxed ">
            {{ csrf_field() }}
            <header><i class="fa fa-users"></i> Forgot Password</header>
            <fieldset>
                <label class="label">E-mail</label>
                <label class="input">
                    <i class="icon-append fa fa-envelope"></i>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                           required>
                    <span class="tooltip tooltip-top-right">Type your Email</span>
                </label>
                <a href="{{ route('office.login') }}">Back to Login</a>
            </fieldset>
            <footer>
                <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-refresh"></i>
                    Reset Passsword
                </button>
            </footer>
        </form>
    </div>
@endsection
