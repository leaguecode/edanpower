@extends('office.layouts.auth')

@section('content')
    <div class="login-box">
        <!-- login form -->
        <form action="{{ route('office.login') }}" method="POST" class="sky-form boxed">
            {{ csrf_field() }}
            <header><i class="fa fa-users"></i> Sign In</header>
            @if ($errors->any())
                <div class="alert alert-mini alert-danger mb-30">
                    <!-- DANGER -->
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <fieldset>
                <section>
                    <label class="label">E-mail</label>
                    <label class="input">
                        <i class="icon-append fa fa-envelope"></i>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                        <span class="tooltip tooltip-top-right">Email Address</span>
                    </label>
                </section>
                <section>
                    <label class="label">Password</label>
                    <label class="input">
                        <i class="icon-append fa fa-lock"></i>
                        <input id="password" type="password" class="form-control" name="password" required>
                        <b class="tooltip tooltip-top-right">Type your Password</b>
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}><i></i>Keep me logged in</label>
                </section>
            </fieldset>
            <footer>
                <button type="submit" class="btn btn-primary pull-right">Sign In</button>
                <div class="forgot-password pull-left">
                    <a href="{{ route('office.password.request') }}">Forgot password?</a>
                </div>
            </footer>
        </form>
    </div>
@endsection
