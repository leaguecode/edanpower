@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
            Waste
        @endslot
    @endcomponent
@endsection
@section('content')
    @component('components.page-title',['title'=>'Waste','links'=>['Services'=>'#','Waste'=>'Waste',]])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <article class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <p class="lead">Many businesses in the UK are still unaware that they are overspending on their waste disposal mechanisms. Whilst there are many reasons, the most common mistakes affecting businesses today are.</p>
                   <p><b>Non-recycling of products</b> | <b>Miscalculating usage</b> | <b>Collection date</b></p>
                    <p>Here at Edan Power, we understand that Waste is a complex market to operate in. That’s why our professional are on hand to do the hard work for you and help you develop a budget strategy that aligns with your business needs</p>
                    <div class="divider divider-center divider-color"><!-- divider -->
                        <i class="fa fa-chevron-down"></i>
                    </div>
                    <p class="lead">A Glimpse into expertise and services we provide within your quoted</p>
                    <ul class="list-unstyled list-icons">
                        <li><i class="fa fa-check text-success"></i><b>Proven track record</b>, helped over 1,000 companies
                        </li>
                        <li><i class="fa fa-check text-success"></i><b>Dedicated</b> customer experience team.
                        </li>
                        <li><i class="fa fa-check text-success"></i><b>Tried and tested</b> suppliers, Constantly monitor wholesale waste market and forward curves in order negotiate the best rates.
                        </li>
                    </ul>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <img class="img-responsive pill-right box-shadow-1"
                         src="{{url('assets/images/pages/waste.jpg')}}" alt="Edan Power Waste">
                </div>
            </article>
        </div>
    </section>

    @component('components.callouts.transparent')
    @endcomponent
@endsection
