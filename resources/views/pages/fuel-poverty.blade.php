@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
Fuel Poverty
        @endslot
    @endcomponent
@endsection

@section('content')
    @component('components.page-title',['title'=>'Fuel Poverty','links'=>['Services'=>'#','Fuel Poverty'=>'fuel-poverty',]])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <h2 >Fuel Poverty</h2>
<p>In the <b>UK</b> the issue of fuel poverty is a serious problem, but one that is set to grow with significant predicted energy price rises over the coming years.
    <b>Fuel poverty</b> is categorised as a household where 10% of its income is spent on energy bills and reports predict that <i>2.9 million households</i> will be in fuel poverty by <b>2016</b>.
</p>
                    <p>It is a problem which will require registered providers of <b>Social Housing</b> to address energy demand and efficiency, but also how energy is being used in homes. <strong>Edanpower</strong> &amp; <b>Partners</b> has developed a range  of technologies to target this problem, ensuring the range of solutions can be applied to both the retrofit market and the new build market.</p>
                    <p><b>Our solutions</b> are easy to deploy, with limited infrastructure or decorating works and are easy to maintain. The technologies are modular ensuring the solution range can be added to in the future to keep increasing savings.
                    </p>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <img class="img-responsive box-shadow-1" src="{{url('assets/images/pages/fuel-poverty.jpg')}}" alt="Edan Power Fuel Poverty">
                </div>

            </div>

        </div>
    </section>
    @include('components.callouts.convinced')
@endsection
