@extends('layouts.app')
@section('meta')
    @component('components.meta')
        @slot('title')
            Contact
        @endslot
    @endcomponent
@endsection
@section('content')
    @component('components.page-title',['title'=>'Contact Us','links'=>['Contact'=>'contact']])
    @endcomponent

    <section class="padding-xxs">

            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-9">
                        <h3>Contact us for more details and information.</h3>
                        <form action="{{route('contact.store')}}" method="post" class="parsley" enctype="multipart/form-data">
                            <fieldset>
                                {{csrf_field()}}
                                <input type="hidden" name="site" value="edanpower">
                                <div class="row">
                                    <div class="col-md-6 form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name">Full Name *</label>
                                        <input type="text" value="{{ old('name') }}" class="form-control" name="name"
                                               id="name" required>
                                    </div>
                                    <div class="col-md-6 form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email">E-mail Address *</label>
                                        <input type="email" value="{{ old('email') }}" class="form-control" name="email"
                                               id="email" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group {{ $errors->has('company') ? ' has-error' : '' }}">
                                        <label for="company">Company Name *</label>
                                        <input type="text" value="{{ old('company') }}" class="form-control"
                                               name="company"
                                               id="company" required>
                                    </div>
                                    <div class="col-md-6 form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="phone">Phone *</label>
                                        <input type="number" value="{{ old('phone') }}" class="form-control"
                                               name="phone"
                                               id="phone" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                                        <label for="message">Message *</label>
                                        <textarea maxlength="10000" rows="6" class="form-control" name="message" required
                                                  id="message">{{old('message')}}</textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="attachment">File Attachment (Like Bills)</label>
                                        <!-- custom file upload -->
                                        <input class="custom-file-upload" type="file" id="attachment" name="files[]"
                                                data-btn-text="Select a File" multiple accept="image/jpeg,application/zip,image/png,application/pdf"/>
                                        <small class="text-muted block">Max file size: 10Mb (zip/pdf/jpg/png)</small>
                                        <div class="alert alert-mini alert-default mb-30">
                                            <ul class="filename">

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> SEND
                                            SEND
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>

                    <div class="col-md-3 col-sm-3">
                        <h2>Visit Us</h2>
                        <p>
                            {{--Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.--}}
                        </p>
                        <hr/>
                        <p>
                            <span class="block">
                            <strong>
                                <i class="fa fa-map-marker"></i>
                                Address:</strong> {{env('ADDRESS')}}</span>
                            <br>
                            <span class="block"><strong><i class="fa fa-phone"></i> Phone:</strong> <a
                                        href="tel:{{env('PHONE')}}">{{env('PHONE')}}</a></span>
                            <br>
                            <span class="block"><strong><i class="fa fa-envelope"></i> Email:</strong>  <a
                                        href="mailto:{{env('EMAIL')}}">{{env('EMAIL')}}</a></span>
                        </p>
                        <p>
                            <a class="btn btn-primary" onclick="myNavFunc()">Get Directions</a>
                        </p>
                        <hr/>
                        <h4 class="font300">Business Hours</h4>
                        <p>
                            <span class="block"><strong>Monday - Friday:</strong> 9:00am to 5:30pm</span>
                            <span class="block"><strong>Saturday & Sunday</strong> Closed</span>
                        </p>
                    </div>

                </div>
                <div id="map3" class="height-300 margin-top-20"></div>
            </div>

    </section>
    @include('components.clients')
@endsection

@section('script')
    {{-- <script type="text/javascript" src="{{url('asset/js/contact.js')}}"></script> --}}
    <script type="text/javascript"
            src="//maps.google.com/maps/api/js?key=AIzaSyDhxvjd0tdMdjID3F-SoJVsqLmnhn7UWrU"></script>
    <script type="text/javascript" src="{{url('assets/plugins/gmaps.js')}}"></script>
    <script type="text/javascript">

        jQuery(document).ready(function () {

            /**
             @BASIC GOOGLE MAP
             **/
            var map2 = new GMaps({
                div: '#map3',
                lat: 52.527525,
                lng: -1.923400,
                zoom: 15,
                scrollwheel: false
            });

            var marker = map2.addMarker({
                lat: 52.527525,
                lng: -1.923400,
                title: 'Edan Power Ltd.',
                infoWindow: {
                    content: '<p><b>Edan Power Ltd.</b></p>'
                }
            });

        });
        $(function() {
            $("input:file").change(function (){
                var files = $(this).prop("files");
                var names = $.map(files, function(val) { return val.name; });
                var items = [];
                $.each(names, function(i, item) {
                    items.push('<li>'+ item +'</li>');
                });
                $('.filename').html('').append( items.join('') );
            });
        });
        function myNavFunc() {
            // If it's an iPhone..
            if ((navigator.platform.indexOf("iPhone") != -1)
                || (navigator.platform.indexOf("iPod") != -1)
                || (navigator.platform.indexOf("iPad") != -1))
                window.open("maps://maps.google.com/maps?q=52.527525,-1.923400");
            else
                window.open("http://maps.google.com/maps?q=52.527525,-1.923400");
        }

    </script>
    {{--<link rel="stylesheet" href="{{url('libs/toastr/toastr.min.css')}}">--}}
    {{--<script src="{{url('libs/toastr/toastr.min.js')}}"></script>--}}

@endsection
