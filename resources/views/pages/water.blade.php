@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
    Water
        @endslot
    @endcomponent
@endsection

@section('content')
    @component('components.page-title',['title'=>'Water','links'=>['Services'=>'#','Water'=>'water',]])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <article class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                  <p class="lead">What is a Water Audits?</p>
                    <p>A water audit is the first potential steps in looking at water savings and water bill rebates due to historical billing errors.</p>
                    <p>Most businesses currently in the <b>UK</b> continue to pay their water bills blind to the fact that reduction in water charges is a high possibility sometimes around <i>30% or more</i>, and there is even a chance the business has been and is still being overcharged we have seen some of our clients get refunds of between <b>&pound;500 - &pound;500,000</b> in the past.</p>
                    <p>Bearing this in mind is your company going to continue paying the water bills without auditing them!!  We should hope not, <strong>Edanpower</strong> regardless if a single site or multiple sited business will help to collect historic Bills for your water and Audit them in order to produce a <b>Preliminary Desktop Water Audit Report</b> with in 7 to 10 days.</p>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <img class="img-responsive pill-right box-shadow-1"
                         src="{{url('assets/images/pages/edanbroker-003.jpg')}}" alt="Edan Power Business Water" >
                </div>
            </article>
        </div>
    </section>
    <section class="alternate">
        <div class="container ">
            <div class="heading-title heading-border-bottom heading-color"><h2 class="nomargin-bottom">
                    Switching Suppliers</h2>
            </div>
            <p>
                Switching your water supplier can save your business between <b>10%</b> to <b>20%</b>, Most companies are now focused on water cost reduction, driven by the need to reduce expenditure and increase margins. Switching water suppliers is just the start of the water cost reduction process.  </p>
            <p>By changing water providers for your sites a substantial cost saving will be achieved.  But currently this is only available in Scotland but will be rolled out across the rest of the <b>UK</b> in <b>2017 </b> so if you are in Scotland contact <strong>Edanpower</strong>.</p>
        </div>
    </section>
    @include('components.callouts.green')
@endsection
