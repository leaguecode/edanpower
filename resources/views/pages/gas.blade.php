@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
            Gas
        @endslot
    @endcomponent
@endsection
@section('content')
    @component('components.page-title',['title'=>'Gas','links'=>['Services'=>'#','Gas'=>'Gas',]])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <article class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <p class="lead">The Business gas supply market has undergone massive changes recently. For many business users the impact has been significant increase in prices and costs. Edanpower will ensure that clients are paying the most competitive business gas rates available.</p>
                    <div class="divider divider-center divider-color"><!-- divider -->
                        <i class="fa fa-chevron-down"></i>
                    </div>
                    <p class="lead">A Glimpse into expertise and services we provide within your quoted</p>
                    <ul class="list-unstyled list-icons">
                        <li><i class="fa fa-check text-success"></i>In depth analysis of your gas usage that will help establish correct tariff you should be paying
                        </li>
                        <li><i class="fa fa-check text-success"></i>We work closely with suppliers, which in turn enable us to take advantage of market conditions, and respond rapidly in order to lock in savings.
                        </li>
                        <li><i class="fa fa-check text-success"></i>Constantly monitor wholesale gas market and forward curves in order negotiate the best rates.
                        </li>
                    </ul>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <img class="img-responsive pill-right box-shadow-1"
                         src="{{url('assets/images/pages/edanbroker-002.jpg')}}" alt="Edan Power Gas">
                </div>
            </article>
        </div>
    </section>

    @component('components.callouts.transparent')
    @endcomponent
@endsection
