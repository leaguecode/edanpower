@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
            Opti Max
        @endslot
    @endcomponent
@endsection

@section('content')
    @component('components.page-title',['title'=>'Opti Max','links'=>['Services'=>'#','Opti Max'=>'opti-plus',]])
    @endcomponent
    <section class="padding-xxs">

            <div class="container">
                <article class="row">
                    <div class="col-md-6">
                        <h2>OPTI-Max</h2>
                        <p class="lead">Want the more enhanced Bolt-on product go for <b>OPTI-Max</b> giving you all the benefits you get with <b>OPTI-Plus</b> but with a fully carbon free heating system,<b> Infra Red Heating</b> the option that warms like the <i>sun</i>.</p>
                        <ul class="list-unstyled list-icons">
                            <li><i class="fa fa-chevron-right text-success"></i> Battery Storage </li>
                            <li><i class="fa fa-chevron-right text-success"></i> LED lighting </li>
                            <li><i class="fa fa-chevron-right text-success"></i> Hot Tap </li>
                            <li><i class="fa fa-chevron-right text-success"></i> Voltage optimisation </li>
                            <li><i class="fa fa-chevron-right text-success"></i> Infra-Red heating </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <img class="img-responsive pill-right box-shadow-1"
                             src="{{url('assets/images/pages/poweringbusinesses-007.jpg')}}" alt="Edan Power Opti Max">
                    </div>
                </article>
            </div>

    </section>
    <!-- / -->
    @include('components.callouts.transparent')
@endsection
