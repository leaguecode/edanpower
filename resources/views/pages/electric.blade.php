@extends('layouts.app')
@section('meta')
    @component('components.meta')
        @slot('title')
            Business Electrics - Birmingham
        @endslot
    @endcomponent
@endsection
@section('content')
    @component('components.page-title',['title'=>'Electric','links'=>['Services'=>'#','Electric'=>'electric',]])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <article class="row">
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <p class="lead">Business electricity supply is an area of expertise that we excel in. We have been helping
                        UK businesses for years and years, we help with negotiating the renewal of clients electricity supply
                        contracts, as well offering the relevant advice on managing the contract while its live.</p>
                    <div class="divider divider-center divider-color"><!-- divider -->
                        <i class="fa fa-chevron-down"></i>
                    </div>
                    <p class="lead">A Glimpse into expertise and services we provide within your quoted</p>
                    <ul class="list-unstyled list-icons">
                        <li><i class="fa fa-check text-success"></i>A deep analysis of your electricity usage data to
                            establish correct tariff rates.
                        </li>
                        <li><i class="fa fa-check text-success"></i>Working very closely with suppliers allows us to
                            take advantage of market conditions allows to lock in future savings.
                        </li>
                        <li><i class="fa fa-check text-success"></i>Constantly monitoring of the wholesale energy market
                            and forward curve of energy.
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <img class="img-responsive pill-right box-shadow-1"
                         src="{{url('assets/images/pages/edanbroker-001.jpg')}}" alt="Edanpower edanbroker Business Electric" >
                </div>
            </article>
        </div>
    </section>
    @component('components.callouts.convinced')
    @endcomponent
@endsection
