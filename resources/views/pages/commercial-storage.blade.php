@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
            Commercial Storage
        @endslot
    @endcomponent
@endsection

@section('content')
    @component('components.page-title',['title'=>'Commercial Storage','links'=>['Services'=>'#','Commercial Storage'=>'commercial-storage',]])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <div class="container">
                <article class="row">
                    <div class="col-md-8">
                        <h3>Energy Storage Solutions for Your Business</h3>
                        <p class="nomargin-bottom">Compatible with all solar PV systems, panels and inverters, the only
                            system ever to efficiently use 100% of the energy generated.</p>
                        <p> Simple plug and play technology enabling easy upgrade of existing equipment.</p>
                        <ul class="list-unstyled list-icons">
                            <li><i class="fa fa-chevron-right text-success"></i> Cut your electricity costs.  
                            </li>
                            <li><i class="fa fa-chevron-right text-success"></i> Stop the effects of spiraling energy
                                costs.
                            </li>
                            <li><i class="fa fa-chevron-right text-success"></i> Minimize your electricity usage from
                                the grid.  
                            </li>
                            <li><i class="fa fa-chevron-right text-success"></i> End your dependency on the electricity
                                grid.
                            </li>
                            <li><i class="fa fa-chevron-right text-success"></i>Reduce your carbon footprint.
                            </li>
                            <li><i class="fa fa-chevron-right text-success"></i> Back-up essential circuits.
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <img class="img-responsive pill-right box-shadow-1"
                             src="{{url('assets/images/pages/poweringbusinesses-002.jpg')}}" alt="Edan Power Storage">
                    </div>
                </article>
            </div>
        </div>
    </section>
    <!-- / -->
    <section class="alternate">
        <div class="container">
            <article class="row">
                <div class="columnize-2 text-center">
                    <h2 class="size-20">Sustainable </h2>
                    <p>Utilises up to 100% of your solar energy. </p>
                    <h2 class="size-20">Simple </h2>
                    <p>Easy to install and expand. </p>
                    <h2 class="size-20">Secure </h2>
                    <p>You’re on when the grid is off. </p>
                    <h2 class="size-20">Storage </h2>
                    <p>Hi-tech, intelligent and expandable. </p>

            </article>
        </div>
    </section>

    @include('components.callouts.convinced')
@endsection

