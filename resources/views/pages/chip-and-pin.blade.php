@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
Chip and Pin
        @endslot
    @endcomponent
@endsection

@section('content')
    @component('components.page-title',['title'=>'Chip and Pin','links'=>['Services'=>'#','Chip and Pin'=>'chip-and-pin',]])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <p class="lead text-center">
                If you would like to start accepting card payments in your business, <a href="{{url('/')}}"><strong><span class="text-success">Edan Power</span></strong></a>
                can help. We deal with a variety of businesses from Market Traders and Independent Stores, to the Blue
                Chips. No matter what your product offering, it is essential in this day and age for businesses to
                accept credit card payments in the easiest and most cost effective way.
            </p>
            <div class="divider divider-center divider-color"><!-- divider -->
                <i class="fa fa-chevron-down"></i>
            </div>

            <article class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <figure class="margin-bottom-20">
                        <img class="img-responsive wow fadeIn"       alt="Edan Power Chip and Pin"   src="{{url('assets/images/pages/card-001.jpg')}}">
                    </figure>
                    <div class="heading-title heading-border-bottom heading-color">
                        <h4 class="nomargin-bottom">Mobile Card Machines </h4></div>
                    <small class="font-lato size-18 margin-bottom-30 block"></small>
                    <p class="text-muted size-14 nomargin-bottom">Mobile card machines are perfect for any business that
                        does not have fixed address.</p>
                    <p class="text-muted size-14">e.g. direct sales companies.</p>

                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <figure class="margin-bottom-20">
                        <img class="img-responsive wow fadeIn"       alt="Edan Power Chip and Pin"   src="{{url('assets/images/pages/card-002.jpg')}}" >
                    </figure>
                    <div class="heading-title heading-border-bottom heading-color">
                        <h4 class="nomargin-bottom">Portable</h4></div>
                    <small class="font-lato size-18 margin-bottom-30 block"></small>
                    <p class="text-muted size-14">Portable card machines are used in business such as restaurants, bars
                        and cafes that require a pay-at-table solution. </p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <figure class="margin-bottom-20">
                        <img class="img-responsive wow fadeIn"       alt="Edan Power Chip and Pin"   src="{{url('assets/images/pages/card-003.jpg')}}" >
                    </figure>
                    <div class="heading-title heading-border-bottom heading-color">
                        <h4 class="nomargin-bottom">Countertop Machines</h4></div>
                    <small class="font-lato size-18 margin-bottom-30 block"></small>
                    <p class="text-muted size-14"> Countertop terminals are a fixed card machine which are suitable for
                        regular retailer businesses success as fast food outlets, supermarkets, and petrol stations. The
                        fixed line card machines connect straight to either your telephone line or your internet
                        connection.</p>
                </div>

            </article>
        </div>
    </section>
    <section class="alternate">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="heading-title heading-border-bottom heading-color">
                        <h3>Advantages of accepting card payments</h3>
                    </div>
                    <p class="nomargin-bottom"><b>Increased profit margins </b>– With 75% of all retail spending that
                        takes place on the high street is using cards</p>
                    <p><b>Quick and efficient payment</b> – Adding credit card payments allow you to offer customers the
                        speedy method of purchase that’s vital to success of any modern business.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="heading-title heading-border-bottom heading-color">
                        <h3>Switch and save upto 50% on Chip and pin</h3>
                    </div>
                    <p>Switching your card machine or your card processing supplier has never been easier you can switch
                        either
                        one or both. By switching your card processing provider, you could get some serious cost savings
                        that
                        will benefit your business. Many Banks charge high processing rates that they then pass onto the
                        customer. Our partners have however negotiated lower transaction fees. </p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h3>Why Switch with us?</h3>
                    <ul class="list-unstyled list-icons">
                        <li><i class="fa fa-check text-success"></i> <b>Switch with us and save up to 50% on rates</b></li>
                        <li><i class="fa fa-check text-success"></i> <b>Fully backed by UK based support </b></li>
                        <li><i class="fa fa-check text-success"></i> <b>Competitive card processing rates</b></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h3>Hassle free switching service</h3>
                    <ul class="list-unstyled list-icons">
                        <li><i class="fa fa-check text-success"></i> <b>Fast, reliable and secure products</b></li>

                    </ul>
                </div>
            </div>
        </div>
    </section>

    @include('components.callouts.parallax-2')
@endsection
