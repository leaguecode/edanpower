@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
            Solar Max
        @endslot
    @endcomponent
@endsection

@section('content')
    @component('components.page-title',['title'=>'Solar Max','links'=>['Services'=>'#','Solar Max'=>'solar-max',]])
    @endcomponent
    <section class="padding-xxs">
            <div class="container">
                <article class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <img class="img-responsive pill-right box-shadow-1"
                             src="{{url('assets/images/pages/poweringbusinesses-009.jpg')}}">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h2>Solar Max</h2>
                        <p class="lead">The ultimate package, all the benefits of <b>Solar Plus</b>, with an added advantage.</p>

                        <ul class="list-unstyled list-icons">
                            <li><i class="fa fa-chevron-right text-success"></i> Solar PV system</li>
                            <li><i class="fa fa-chevron-right text-success"></i> Battery Storage</li>
                            <li><i class="fa fa-chevron-right text-success"></i> LED lighting</li>
                            <li><i class="fa fa-chevron-right text-success"></i> Hot Tap</li>
                            <li><i class="fa fa-chevron-right text-success"></i> Voltage optimisation</li>
                            <li><i class="fa fa-chevron-right text-success"></i> Infra-Red heating</li>
                        </ul>
                    </div>
                    <div class="col-md-12 text-center">
                        <hr>
                        <p class="lead">Not only will you reap the saving that <b>Solar Plus</b> will give you,  <b>Solar Max</b> will reduce your heating costs with the <i>modern Zero Carbon Infra Red heating system</i>. Solar Ready have a full range of <b>AC</b> and <b>DC</b> powered <i>IR panels</i> in a number of finishes. Reduce energy consumption by up to <b>90%</b>.</p>
                    </div>
                </article>
            </div>

    </section>
    <!-- / -->
    @include('components.callouts.green')
@endsection
