@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
Opti Plus
        @endslot
    @endcomponent
@endsection

@section('content')
    @component('components.page-title',['title'=>'Opti Plus','links'=>['Services'=>'#','Opti Plus'=>'opti-plus',]])
    @endcomponent
    <section class="padding-xxs">
            <div class="container">
                <article class="row">
                    <div class="col-md-6">
                        <img class="img-responsive pill-right box-shadow-1"
                             src="{{url('assets/images/pages/poweringbusinesses-007.jpg')}}" alt="Edan Power Opti Plus">
                    </div>
                    <div class="col-md-6">
                        <h2>OPTI-Plus</h2>
                        <p class="lead">Already have a <b>Solar PV</b> system, well maximise the benefits with our Bolt-on products, massive further <b>saving</b>, make your Solar PV system work round the clock instead of just <b>daylight hours</b>.</p>

                        <ul class="list-unstyled list-icons">
                            <li><i class="fa fa-chevron-right text-success"></i> Battery Storage </li>
                            <li><i class="fa fa-chevron-right text-success"></i> LED lighting </li>
                            <li><i class="fa fa-chevron-right text-success"></i> Hot Tap </li>
                            <li><i class="fa fa-chevron-right text-success"></i> Voltage Optimisation </li>
                        </ul>
                    </div>
                </article>
            </div>

    </section>
    <!-- / -->
    @include('components.callouts.green')
@endsection
