@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
Telecoms
        @endslot
    @endcomponent
@endsection

@section('content')
    @component('components.page-title',['title'=>'Telecoms','links'=>['Services'=>'#','Telecoms'=>'telecoms',]])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
        <article class="row">
            {{--<div class="col-lg-6 col-md-6 col-md-6">--}}
                <div class="heading-title heading-border-bottom heading-color">
                    <h3>Edan Power's rates and reliable lines</h3>
                </div>
                <p>Phone lines and the calls are the most basic of the services we offer, but for many customers obtaining the correct telecoms package is a critical decision. For your peace of mind our services run over the same physical networks that covers the whole of the UK by other market leaders that own the infrastructure.</p>
            {{--</div>--}}
            {{--<div class="col-lg-6 col-md-6 col-md-6"></div>--}}
        </article>
        </div>
    </section>
    <section class="alternate">
        <div class="container">
            <article class="row">
                {{--<div class="col-lg-6 col-md-6 col-md-6"></div>--}}
                {{--<div class="col-lg-6 col-md-6 col-md-6">--}}
                    <div class="heading-title heading-border-bottom heading-color">
                        <h3>Line Rental and Calls</h3>
                    </div>
                    <p>When you ask us to take over existing lines, or provide new ones we become the service provider. Edan Power offer fantastic rates on existing and new phone lines, with excellent call rates. Everything else stays the same, including the telephone number. We offer standard exchange lines, ISDN2 lines, ISDN30 lines as well as VoIP lines.  </p>
                    <p class="nomargin-bottom"><b>Key Benefits</b> </p>
                    <div >
                        <ul class="list-unstyled list-icons">
                            <li><i class="fa fa-check text-success"></i>A trusted business grade service </li>
                            <li><i class="fa fa-check text-success"></i>Competitively priced £12.99 per month</li>
                            <li><i class="fa fa-check text-success"></i>Low call rates</li>
                            <li><i class="fa fa-check text-success"></i>Enhanced business quality </li>
                            <li><i class="fa fa-check text-success"></i>Performance </li>
                        </ul>
                    </div>
                {{--</div>--}}

            </article>
        </div>
    </section>
    <section>
        <div class="container">
            <article class="row">
                {{--<div class="col-lg-6 col-md-6 col-md-6">--}}
                    <div class="heading-title heading-border-bottom heading-color">
                        <h3>Broadband</h3>
                    </div>
                    <p>We offer up to 8Mb* broadband connections which are 160 times faster than standard dial-up internet.  The key benefit our faster broadband connection is the opportunity to maintain greater quality when multi-tasking. Our service has an ‘always on’, high speed connectivity across a solid network that is supported by a specialist technical support team. </p>
                    <p class="nomargin-bottom"><b>Key Benefits</b> </p>

                        <ul class="list-unstyled list-icons">
                            <li><i class="fa fa-check text-success"></i>A trusted business grade service</li>
                            <li><i class="fa fa-check text-success"></i>Competitively priced Broadband</li>
                            <li><i class="fa fa-check text-success"></i>Enhanced business quality</li>
                            <li><i class="fa fa-check text-success"></i>Performance</li>
                            <li><i class="fa fa-check text-success"></i>24 x 7 Internet access</li>
                            <li><i class="fa fa-check text-success"></i>No proxy servers</li>
                        </ul>


                {{--</div>--}}
                {{--<div class="col-lg-6 col-md-6 col-md-6"></div>--}}
            </article>
        </div>
    </section>
    @include('components.callouts.parallax-2')
@endsection
