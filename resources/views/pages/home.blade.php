@extends('layouts.app')
@section('meta')
    @component('components.meta')
    @endcomponent
@endsection

@section('content')
    @include('components.home-slider')
    <section class="padding-xs">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-push-6">
                    <div class="heading-title heading-border heading-color margin-top-0">

                        <h1 class="weight-400">Welcome to EDANPOWER</h1>
                       
                    </div>
                    <p class="lead">Edan Power are committed to changing the way businesses and homeowners currently
                        source their energy and communication requirements, via new greener solutions that also provide
                        significant cost savings for businesses and residential dwellings.</p>
                </div>
                <div class="col-lg-6 col-lg-pull-6">
                    <div class="embed-responsive embed-responsive-16by9 margin-bottom-60">
                        <iframe id="edanPowerVideoAd" class="embed-responsive-item" width="560" height="315"
                                src="https://www.youtube.com/embed/nsnhIoU6Dt4?rel=0&amp;controls=0&amp;showinfo=0"
                                allowfullscreen></iframe>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="alternate">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-5th">
                    <a href="{{route('electric')}}">
                        <div class="thumbnail">
                            <img class="img-responsive"
                                 src="{{asset('assets/images/pages/poweringbusinesses-006.jpg')}}"
                                 alt="Edan Power Electric" height=""/>
                            <div class="caption">
                                <h3 class="nomargin">Electric</h3>
                                <small class="margin-bottom-20 block">Home & Business Electricity</small>
                                <p>Home & Business Electricity supply is an area of expertise that we excel in. We have...</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-5th">
                    <a href="{{route('water')}}">
                        <div class="thumbnail">
                            <img class="img-responsive" src="{{asset('assets/images/pages/edanbroker-003.jpg')}}"
                                 alt="Edan Power Water"/>
                            <div class="caption">
                                <h3 class="nomargin">Water</h3>
                                <small class="margin-bottom-20 block">Home & Business Water</small>
                                <p>Home & Business Water supply is an area of expertise that we excel in. We have been
                                    helping UK...</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-5th">
                    <a href="{{route('gas')}}">
                        <div class="thumbnail">
                            <img class="img-responsive" src="{{asset('assets/images/pages/edanbroker-002.jpg')}}"
                                 alt="Edan Power Gas"/>
                            <div class="caption">
                                <h3 class="nomargin">Gas</h3>
                                <small class="margin-bottom-20 block">Home & Business Gas</small>
                                <p>Home & Business Gas supply market has undergone massive changes recently. For many...</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-5th">
                    <a href="{{route('telecoms')}}">
                        <div class="thumbnail">
                            <img class="img-responsive" src="{{asset('assets/images/pages/sustainable-energy.jpg')}}"
                                 alt="Edan Power Telecoms"/>
                            <div class="caption">
                                <h3 class="nomargin">Telecoms</h3>
                                <small class="margin-bottom-20 block">Home & Business Telecoms</small>
                                <p>Home & Business Phone lines and the calls are the most basic of the services we offer...</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-5th">
                    <a href="{{route('chipAndPin')}}">
                        <div class="thumbnail">
                            <img class="img-responsive" src="{{asset('assets/images/pages/card-001.jpg')}}" style="height: 136px"  alt="Edan Power Chip And Pin"/>
                            <div class="caption">
                                <h3 class="nomargin">Chip and Pin</h3>
                                <small class="margin-bottom-20 block">Card payments</small>
                                <p>Mobile, Countertop, WI-FI, Bluetooth, Contactless card payments for you
                                    Business...</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    @include('components.callouts.parallax-2')
    @include('components.clients')



@endsection