@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
Solar Plus
        @endslot
    @endcomponent
@endsection

@section('content')
    @component('components.page-title',['title'=>'Solar Plus','links'=>['Services'=>'#','Solar Plus'=>'solar-plus',]])
    @endcomponent
    <section class="padding-xxs">
            <div class="container">
                <article class="row">
                    <div class="col-md-6">
                        <p class="lead">The pack that will save you thousands of pounds.</p>

                        <ul class="list-unstyled list-icons">
                            <li><i class="fa fa-chevron-right text-success"></i> Solar PV system
                            </li>
                            <li><i class="fa fa-chevron-right text-success"></i> Battery Storage
                            </li>
                            <li><i class="fa fa-chevron-right text-success"></i> LED lighting
                            </li>
                            <li><i class="fa fa-chevron-right text-success"></i> Hot Tap
                            </li>
                            <li><i class="fa fa-chevron-right text-success"></i> Voltage optimisation
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <img class="img-responsive pill-right box-shadow-1"
                             src="{{url('assets/images/pages/poweringbusinesses-001.jpg')}}">
                    </div>
                    <div class="col-md-12 text-center">
                        <hr>
                        <p class="lead">Modernising and future proofing your home all at the sometime, Massive savings
                            from this system will protect you from greedy energy companies and their energy price
                            increases. </p>
                    </div>
                </article>
            </div>

    </section>

    @include('components.callouts.green')
@endsection
