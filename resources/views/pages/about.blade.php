@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
            About
        @endslot
    @endcomponent
@endsection

@section('content')
    @component('components.page-title',['title'=>'About','links'=>['About'=>'about']])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">

            <div class="row">

                <div class="col-lg-12">

                    <div class="heading-title">
                        <h2>Who We Are?</h2>
                    </div>
                    <p><strong>Edan Power</strong> are committed to changing the way businesses and homeowners currently
                        source their energy and communication requirements, via new greener solutions that also provide
                        significant cost savings for businesses and residential dwellings. Our contribution to this
                        revolution is developing strategic partnerships within the energy, renewable, finance and other
                        sectors with market leading organisations providing the optimum solution for your requirements.
                    </p>
                    <img class="pull-right" src="{{asset('assets/images/pages/about-us.png')}}" alt="Edan Power About Us"/>
                    <p> We offer our customers a total turnkey solution providing a strategic development, financing,
                        design, installation, monitoring and maintenance package. We provide a consultative service
                        utilising all available funding opportunities, including grants and investor schemes, often
                        with no upfront Capex investment required.</p>
                    <p>The ever increasing risk of Increases energy bills for <strong>UK</strong> business and <strong>homeowners</strong>
                        means more and more people are looking to find alternative solutions to reduce their energy
                        bills. The commitment by all world governments to reduce reliance on traditional fossil fuels due
                        to global warming, is significantly increasing the demand for renewable energy and all
                        associated technologies.</p>
                    <p>Our aim is to provide a sustainable future for the world our future generations, with bespoke and
                        cost effective options for our clients</p>


                </div>


            </div>

        </div>
    </section>
    <section class="alternate">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <img class="img-responsive box-shadow-1" src="{{asset('assets/images/pages/our-partners.jpg')}}" alt="Edan Power Our Partners" />
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="heading-title">
                        <h2>Our Strategic Business Partners</h2>
                    </div>
                    <p><strong>Edan Power</strong> believes in developing business partners that pride themselves on ensuring that our clients receive the best service in all aspect of business procurement, consultancy, installation and on-going support.  We work in collaboration with very high level businesses and best calibre consultants to bring our clients the best value.</p>
                </div>
            </div>
        </div>

        <hr/>

        <div class="container">
            <h2>Our Partners</h2>
            <ul class="row clients-dotted list-inline">
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/britishgas.png')}}" alt="Edan Power Partners - British Gas"></a></li>
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/barclays.png')}}" alt="Edan Power Partners - Barclays"></a></li>
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/d-energi.png')}}" alt="Edan Power Partners - D Energi"></a></li>
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/engie.png')}}" alt="Edan Power Partners - Engie"></a></li>
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/totalgp.png')}}" alt="Edan Power Partners - Total GP"></a></li>
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/cng.png')}}" alt="Edan Power Partners - CNG"></a></li>
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/e-on.png')}}" alt="Edan Power Partners - E-ON"></a></li>
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/yu-energy.png')}}"  alt="Edan Power Partners - YU Energy"></a></li>
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/gazprom.png')}}" alt="Edan Power Partners - Gazprom"></a></li>
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/npower.png')}}" alt="Edan Power Partners - nPower"></a></li>
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/opusenergy.png')}}" alt="Edan Power Partners - Opus Energy"></a></li>
                <li class="col-md-3 col-sm-3 col-xs-6"><a class="img-hover"><img class="img-responsive" src="{{asset('assets/images/clients/havenpower.png')}}" alt="Edan Power Partners - Haven Power"></a></li>


            </ul>
        </div>
    </section>
@component('components.callouts.parallax-2')
    @endcomponent
@endsection
