@extends('layouts.app')

@section('meta')
    @component('components.meta')
        @slot('title')
            Commercial Led
        @endslot
    @endcomponent
@endsection

@section('content')
    @component('components.page-title',['title'=>'Commercial Led','links'=>['Services'=>'#','Commercial Led'=>'commercial-led',]])
    @endcomponent
    <section class="padding-xxs">
        <div class="container">
            <article class="row">
                <div class="col-md-4"><h3>OUR TARGET</h3>
                    <p>Operating lease for 5 years with no cost of investment. </p>
                    <p>Savings from year 1 with typical 25% share of total savings year 1 and full savings after 5
                        years.</p>
                </div>
                <div class="col-md-4">
                    <h3>ENERGY USAGE</h3>
                    <p>Offices are open 5 days equalling 50 hours a week for 50 weeks a year - This means
                        <i>2,500</i> hours usage a year from a bulb with a lifetime of <b>50,000 hours = 20 years
                            life </b>- Alternative lighting needs replacement every <b>6 years</b>
                    </p>
                </div>
                <div class="col-md-4">
                    <img class="img-responsive pill-right box-shadow-1"
                         src="{{url('assets/images/pages/poweringbusinesses-006.jpg')}}" alt="Edan Power Commercial Led">
                </div>
            </article>
            <div class="divider divider-circle divider-color divider-center divider-short"><!-- divider -->
                <i class="fa fa-chevron-down"></i>
            </div>

            <article class="row">
                <div class="col-md-6">
                    <h3>Advantages of LED </h3>
                    <ul class="list-unstyled list-icons">
                        <li><i class="fa fa-check text-success"></i> Saving of <b>60% to 70%</b> in electricity
                            costs.
                        </li>
                        <li><i class="fa fa-check text-success"></i> The Maintenance of the light are simpler as the
                            lifetime of the bulb is 3 to 4 times longer therefore saving on regular maintenance
                            costs and constant bulb replacement costs.
                        </li>
                        <li><i class="fa fa-check text-success"></i> LED bulbs have a lifetime of 50,000 hours of
                            use compared to <b>15,000</b> for the fluorescent alternative.
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <h3>Disadvantages of LED</h3>
                    <ul class="list-unstyled list-icons">
                        <li><i class="glyphicon glyphicon-remove"></i> The Initial cost is 5 times more that of
                            alternative fluorescent lamps.
                        </li>
                        <li><i class="glyphicon glyphicon-remove"></i> Ideally for payback on LED (2 to 2 years)
                            need Minimum usage of 35 hours a week. The higher Weekly usage the higher return.
                        </li>
                    </ul>
                </div>
            </article>
            <div class="divider divider-center divider-color"><!-- divider -->
                <i class="fa fa-chevron-down"></i>
            </div>
        </div>
    </section>

    <section class="alternate">
        <div class="container">

            <article class="row">
                <div class="col-md-6">

                    <div class="owl-carousel buttons-autohide controlls-over nomargin"
                         data-plugin-options='{"items": 1, "autoHeight": true, "navigation": true, "pagination": true, "transitionStyle":"backSlide", "progressBar":"true"}'>
                        <div>
                            <img class="img-responsive" src="{{url('assets/images/pages/slider/led-001.jpg')}}" alt="Edan Power Led">
                        </div>
                        <div>
                            <img class="img-responsive" src="{{url('assets/images/pages/slider/led-002.jpg')}}" alt="Edan Power Led">
                        </div>
                        <div>
                            <img class="img-responsive" src="{{url('assets/images/pages/slider/led-003.jpg')}}" alt="Edan Power Led">
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <h3>Energy Savings Information</h3>
                    <p class="lead">Coney Green Business Centre, Chesterfield</p>
                    <p class="nomargin-bottom"><b>&pound;1,054.69</b> Old Monthly Lighting Energy Costs </p>
                    <p class="nomargin-bottom"><b>&pound;362.59</b> New Monthly Lighting Energy Costs </p>
                    <p><b>&pound;8,305.18</b> First Year Estimated Savings</p>

                </div>
            </article>

        </div>
    </section>
@include('components.callouts.parallax-2')
<section>
    <div class="container">
        <article class="row">
            <div class="col-md-4">
                <h3>Enhanced Capital Allowances </h3>
                <p>The <b>government technology</b> list now includes White LED Lighting. This means that some of our LED product range qualifies for <b>Enhanced Capital Allowances</b> (ECA). </p>
            </div>
            <div class="col-md-4">
                <h3>Reduced Maintenance Cost </h3>
                <p>There is a reduced cost of maintenance as our T8 LED Tubes require no starters or ballasts. </p>
            </div>
            <div class="col-md-4">
                <h3>Emergency Lighting </h3>
                <p>LED emergency lighting is now starting to become very popular thanks to its energy saving benefits. A lot of illuminated exit signs are now LED and are virtually maintenance free. This is thanks to the fact that the LEDs in the fitting will last for around 50,000 hours without needing to be replaced.</p>
            </div>
        </article>
    </div>
</section>

    @include('components.callouts.convinced')
@endsection

