<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h3>Drop us a line or just say <strong><em>Hello!</em></strong> and upload your bills.</h3>
            <form action="{{url('/')}}" method="post" class="parsley" enctype="multipart/form-data" id="contact">
                <fieldset>
                    {{csrf_field()}}
                    <input type="hidden" name="site" value="{{Request::server("HTTP_HOST")}}">
                    <div class="row">
                        <div class="col-md-6 form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Full Name *</label>
                            <input type="text" value="{{ old('name') }}" class="form-control" name="name"
                                   id="name" required>
                        </div>
                        <div class="col-md-6 form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">E-mail Address *</label>
                            <input type="email" value="{{ old('email') }}" class="form-control" name="email"
                                   id="email" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group {{ $errors->has('company') ? ' has-error' : '' }}">
                            <label for="company">Company Name *</label>
                            <input type="text" value="{{ old('company') }}" class="form-control"
                                   name="company"
                                   id="company" required>
                        </div>
                        <div class="col-md-6 form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="phone">Phone *</label>
                            <input type="number" value="{{ old('phone') }}" class="form-control"
                                   name="phone"
                                   id="phone" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                            <label for="message">Message *</label>
                            <textarea maxlength="10000" rows="6" class="form-control" name="message" required
                                      id="message">{{old('message')}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="attachment">File Attachment</label>
                            <!-- custom file upload -->
                            <input class="custom-file-upload" type="file" id="attachment" name="files[]"
                                   data-btn-text="Select a File" multiple
                                   accept="image/jpeg,application/zip,image/png,application/pdf"/>
                            <small class="text-muted block">Max file size: 10Mb (zip/pdf/jpg/png)</small>
                            <div class="alert alert-mini alert-default mb-30">
                                <ul class="filename">

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> SEND
                                MESSAGE
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>