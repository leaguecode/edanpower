@extends('layouts.partners-app')
@section('meta')
    @component('components.meta-partners')
        @slot('title')
            Edan Cover
        @endslot
        @slot('description')
            Edan Cover are committed to changing the way businesses and homeowners currently source their covering insurance, via new greener solutions that also provide significant cost savings for businesses and residential dwellings.
        @endslot
        @slot('keywords')
            edancover, edan cover, edan cover insurance, edan insurance, birmingham edan cover insurance, birmingham edan insurance, birmingham uk edan cover insurance, birmingham uk edan insurance, birmingham uk edan cover  business insurance, birmingham uk edan business insurance, business insurance, edan business insurance, birmingham edan business insurance,
        @endslot
        @slot('image')
            {{asset('assets/images/edan/cover.jpg')}}
        @endslot
    @endcomponent
    <meta name="Author" content="EdanCover"/>
    <meta name="google-site-verification" content="q6H4x1E_Ftnw5hxQahliQ-g6g4QjtsVYJtyOcTrwF0A" />
@endsection
@section('style')
    <link href="{{asset('assets/plugins/slider.revolution.v5/css/pack.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

    <div id="rev_slider_56_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container"
         data-alias="sports-hero54"
         style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
        <div id="rev_slider_56_1" class="rev_slider fullwidthabanner" style="display:none;"
             data-version="5.0.7">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-214" data-transition="fade" data-slotamount="7" data-easein="default"
                    data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off"
                    data-title="Slide" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('assets/images/pages/insurance.jpg')}}" alt=""
                         data-bgposition="center center"
                         data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg"
                         data-no-retina>
                    <div class="overlay dark-6">
                        <!-- dark overlay [1 to 9 opacity] -->
                    </div>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption Sports-Display   tp-resizeme rs-parallaxlevel-0"
                         id="slide-214-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['-170','-170','-190','-140']"
                         data-fontsize="['130','130','130','100']" data-lineheight="['130','130','130','100']"
                         data-width="none" data-height="none" data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:2000;e:Power3.easeInOut;"
                         data-transform_out="y:[100%];s:1000;s:1000;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="750"
                         data-splitin="none"
                         data-splitout="none" data-responsive_offset="on"
                         style="z-index: 5; white-space: nowrap;">
                        EDAN
                    </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Sports-DisplayFat   tp-resizeme rs-parallaxlevel-0"
                         id="slide-214-layer-2"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-48','-48','-68','-48']"
                         data-fontsize="['130','130','130','100']" data-lineheight="['130','130','130','100']"
                         data-width="none" data-height="none" data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:2000;e:Power3.easeInOut;"
                         data-transform_out="y:[100%];s:1000;s:1000;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                         data-splitin="none"
                         data-splitout="none" data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;">
                        COVER
                    </div>
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-0" id="slide-214-layer-3"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['70','70','50','40']"
                         data-width="none" data-height="none" data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="y:[100%];s:1000;s:1000;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1250"
                         data-responsive_offset="on" style="z-index: 7;"><img
                                src="{{asset('assets/images/sports_sublinebg.png')}}" alt="" width="500"
                                height="45"
                                data-ww="['700px','500px','500px','420px']" data-hh="45px" data-no-retina>
                    </div>
                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption Sports-Subline   tp-resizeme rs-parallaxlevel-0"
                         id="slide-214-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['71','71','51','41']"
                         data-fontsize="['30','30','30','25']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;"
                         data-transform_out="y:[100%];s:1000;s:1000;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500"
                         data-splitin="chars"
                         data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05"
                         style="z-index: 8; white-space: nowrap; font-size: 20px; line-height: 30px;">POWERING
                        YOUR HOME AND BUSINESS
                    </div>
                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption rev-btn rev-bordered   rs-parallaxlevel-0" id="slide-214-layer-6"
                         data-x="['center','center','center','center']" data-hoffset="['-180','-180','0','0']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['120','70','115','115']"
                         data-width="none" data-height="none" data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:500;e:Linear.easeNone;"
                         data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);"
                         data-transform_in="x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:[100%];s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2500"
                         data-splitin="none"
                         data-splitout="none"
                         data-actions='[{"event":"click","action":"scrollbelow","offset":"px"}]'
                         data-responsive_offset="off" data-responsive="off"
                         style="z-index: 10; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 600; color: rgba(255, 255, 255, 1.00);font-family:Raleway;background-color:rgba(0, 0, 0, 0);padding:12px 35px 12px 35px;border-color:rgba(255, 255, 255, 0.50);border-style:solid;border-width:2px;letter-spacing:2px;">
                        EXPLORE EDANCOVER
                    </div>
                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption rev-btn rev-bordered   rs-parallaxlevel-0" id="slide-214-layer-7"
                         data-x="['center','center','center','center']" data-hoffset="['180','180','0','0']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['120','70','50','50']"
                         data-width="none" data-height="none" data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:500;e:Linear.easeNone;"
                         data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(0, 0, 0, 1.00);bc:rgba(0, 0, 0, 1.00);"
                         data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:[-100%];s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2500"
                         data-splitin="none"
                         data-splitout="none"
                         data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"#contact","offset":"px"}]'
                         data-responsive_offset="off" data-responsive="off"
                         style="z-index: 11; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 600; color: rgba(255, 255, 255, 1.00);font-family:Raleway;background-color:rgba(219, 28, 34, 1.00);padding:12px 35px 12px 35px;border-color:rgba(219, 28, 34, 0);border-style:solid;border-width:2px;letter-spacing:2px;">
                        CONTACT NOW
                    </div>
                </li>
            </ul>
            <div class="tp-static-layers"></div>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
    <section>
        <div class="container">
            <div class="text-center">
                <h1 class="font-lato">Welcome to <span>Edan Cover</span></h1>
                <h2 class="col-sm-10 col-sm-offset-1 nomargin-bottom weight-400">
                    Edan Cover are committed to changing the way businesses and homeowners currently source their
                    covering insurance, via new greener solutions that also provide significant cost savings for
                    businesses and residential dwellings.
                </h2>
            </div>
        </div>
    </section>
    @include('components.callouts.parallax-2')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <header class="margin-bottom-60">
                        <h2>Benefits with Edan Cover</h2>
                    </header>
                    <div class="toggle toggle-transparent toggle-bordered-full">
                        <div class="toggle active">
                            <label>Make significant cost savings</label>
                            <div class="toggle-content">
                             <p>​Many businesses are overspending on their insurance. Our specialists will find the most cost-effective and appropriate deals for you.</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>Access specialist market cover</label>
                            <div class="toggle-content">
                                <p>Through our team, you can access specialist market cover for your trade, including public liability for the pub, sports or retail industries.</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>Remove the stress</label>
                            <div class="toggle-content">
                               <p>​Choosing the correct policy is time-consuming and stressful. Our team will do the hard work and let you get back to doing what you do best: running your business.</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>Increase job security</label>
                            <div class="toggle-content">
                           <p>Don’t let choosing the wrong policy come back to haunt you in an emergency. Our team will ensure you make the right choice for your business.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <header class="margin-bottom-60">
                        <h2>Why Edan Cover?</h2>
                    </header>
                    <div class="toggle toggle-transparent toggle-bordered-full">
                        <div class="toggle active">
                            <label>Proven track record</label>
                            <div class="toggle-content">
                               <p>We’ve helped over 1,500 companies make savings across their business, with a 98% recommendation rate.</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>Dedicated customer experience team</label>
                            <div class="toggle-content">
                            <p> We will protect you every step of the way, ensuring you continue to receive the best prices when your contracts are up for renewal.</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>Tried and tested suppliers</label>
                            <div class="toggle-content">
                               <p>We only work with the very best. You, the customer, are what’s most important to us and saving you money is our number one priority. We make sure that we not only get you the cheapest deal, but also one that is tailor-made for your business.</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>More services than anyone else</label>
                            <div class="toggle-content">
                               <p>We don’t just stop with your business insurance, we can make you savings on over 12 different areas of your business, more than any of our competitors.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    @include('components.callouts.transparent-2')
    <section class="alternate-2" id="contact">
    @include('partners.contact-form')
    </section>
    <footer id="footer">
        <div class="copyright">
            <div class="container">
                <ul class="pull-right nomargin list-inline mobile-block">
                    <li><a href="#">Terms &amp; Conditions</a></li>
                    <li>&bull;</li>
                    <li><a href="#">Privacy</a></li>
                </ul>
                {{date('Y')}} &copy; All Rights Reserved, Edan Power Ltd.
            </div>
        </div>
    </footer>
@endsection
@section('script')
    <script type="text/javascript"
            src="{{asset('assets/plugins/slider.revolution.v5/js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('assets/plugins/slider.revolution.v5/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            if (jQuery("#rev_slider_56_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_56_1");
            } else {
                revapi56 = jQuery("#rev_slider_56_1").show().revolution({
                    sliderType: "hero",
                    jsFileLocation: plugin_path + "slider.revolution.v5/js/",
                    sliderLayout: "fullwidth",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {},
                    responsiveLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [720, 640, 640, 640],
                    lazyType: "none",
                    parallax: {
                        type: "scroll",
                        origo: "enterpoint",
                        speed: 400,
                        levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                    },
                    shadow: 0,
                    spinner: "off",
                    autoHeight: "off",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        disableFocusListener: false,
                    }
                });
            }
        });
        $(function() {
            $("input:file").change(function (){
                var files = $(this).prop("files");
                var names = $.map(files, function(val) { return val.name; });
                var items = [];
                $.each(names, function(i, item) {
                    items.push('<li>'+ item +'</li>');
                });
                $('.filename').html('').append( items.join('') );
            });
        });
    </script>
    @include('components.analytics',['id'=>'UA-104769260-2'])
@endsection
