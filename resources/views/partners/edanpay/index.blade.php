@extends('layouts.partners-app')
@section('meta')
    @component('components.meta-partners')
        @slot('title')
            Edan Pay
        @endslot
        @slot('description')
            Edan Pay are committed to changing the way businesses and homes currently source their payments services, via new pioneer solutions that also provide significant cost savings for businesses.
        @endslot
        @slot('keywords')
            Mobile Card Machine, Portable Card Machine, Countertop Card Machine, Debit & Credit Card Machines, Wi-Fi & Portable, Contactless Card Machines, Apple Pay, Android Pay & Samsung Pay, e-commerce payments, online payments, payment gateway, business Mobile Card Machine, business Portable Card Machine, business Countertop Card Machine, business Debit & Credit Card Machines, business Wi-Fi & Portable, business Contactless Card Machines, business Apple Pay, business Android Pay & Samsung Pay, business e-commerce payments, business online payments, business payment gateway, business uk birmingham Mobile Card Machine, business uk birmingham Portable Card Machine, business uk birmingham Countertop Card Machine, business uk birmingham Debit & Credit Card Machines, business uk birmingham Wi-Fi & Portable, business uk birmingham Contactless Card Machines, business uk birmingham Apple Pay, business uk birmingham Android Pay & Samsung Pay, business uk birmingham e-commerce payments, business uk birmingham online payments, business uk birmingham payment gateway, edan pay business uk birmingham Mobile Card Machine, edan pay business uk birmingham Portable Card Machine, edan pay business uk birmingham Countertop Card Machine, edan pay business uk birmingham Debit & Credit Card Machines, edan pay business uk birmingham Wi-Fi & Portable, edan pay business uk birmingham Contactless Card Machines, edan pay business uk birmingham Apple Pay, edan pay business uk birmingham Android Pay & Samsung Pay, edan pay business uk birmingham e-commerce payments, edan pay business uk birmingham online payments, edan pay business uk birmingham payment gateway,
        @endslot
        @slot('image')
            {{asset('assets/images/edan/pay.jpg')}}
        @endslot
    @endcomponent
    <meta name="Author" content="EdanPay"/>
    <meta name="google-site-verification" content="fFCIDWsuBIMEP6lGzthb0Td935h0yEO62GVx5hqz7Gg" />
@endsection
@section('style')
    <link href="{{asset('assets/plugins/slider.revolution.v5/css/pack.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

    <div id="rev_slider_56_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container"
         data-alias="sports-hero54"
         style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
        <div id="rev_slider_56_1" class="rev_slider fullwidthabanner" style="display:none;"
             data-version="5.0.7">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-214" data-transition="fade" data-slotamount="7" data-easein="default"
                    data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off"
                    data-title="Slide" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('assets/images/pages/payments.jpg')}}" alt=""
                         data-bgposition="center center"
                         data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg"
                         data-no-retina>
                    <div class="overlay dark-6">
                        <!-- dark overlay [1 to 9 opacity] -->
                    </div>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption Sports-Display   tp-resizeme rs-parallaxlevel-0"
                         id="slide-214-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['-170','-170','-190','-140']"
                         data-fontsize="['130','130','130','100']" data-lineheight="['130','130','130','100']"
                         data-width="none" data-height="none" data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:2000;e:Power3.easeInOut;"
                         data-transform_out="y:[100%];s:1000;s:1000;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="750"
                         data-splitin="none"
                         data-splitout="none" data-responsive_offset="on"
                         style="z-index: 5; white-space: nowrap;">
                        EDAN
                    </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Sports-DisplayFat   tp-resizeme rs-parallaxlevel-0"
                         id="slide-214-layer-2"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-48','-48','-68','-48']"
                         data-fontsize="['130','130','130','100']" data-lineheight="['130','130','130','100']"
                         data-width="none" data-height="none" data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:2000;e:Power3.easeInOut;"
                         data-transform_out="y:[100%];s:1000;s:1000;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                         data-splitin="none"
                         data-splitout="none" data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;">
                        PAY
                    </div>
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-0" id="slide-214-layer-3"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['70','70','50','40']"
                         data-width="none" data-height="none" data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="y:[100%];s:1000;s:1000;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1250"
                         data-responsive_offset="on" style="z-index: 7;"><img
                                src="{{asset('assets/images/sports_sublinebg.png')}}" alt="" width="400"
                                height="45"
                                data-ww="['700px','500px','500px','420px']" data-hh="45px" data-no-retina>
                    </div>
                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption Sports-Subline   tp-resizeme rs-parallaxlevel-0"
                         id="slide-214-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['71','71','51','41']"
                         data-fontsize="['30','30','30','25']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;"
                         data-transform_out="y:[100%];s:1000;s:1000;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500"
                         data-splitin="chars"
                         data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05"
                         style="z-index: 8; white-space: nowrap; font-size: 20px; line-height: 30px;">POWERING
                        YOUR BUSINESS
                    </div>
                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption rev-btn rev-bordered   rs-parallaxlevel-0" id="slide-214-layer-6"
                         data-x="['center','center','center','center']" data-hoffset="['-180','-180','0','0']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['120','70','115','115']"
                         data-width="none" data-height="none" data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:500;e:Linear.easeNone;"
                         data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);"
                         data-transform_in="x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:[100%];s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2500"
                         data-splitin="none"
                         data-splitout="none"
                         data-actions='[{"event":"click","action":"scrollbelow","offset":"px"}]'
                         data-responsive_offset="off" data-responsive="off"
                         style="z-index: 10; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 600; color: rgba(255, 255, 255, 1.00);font-family:Raleway;background-color:rgba(0, 0, 0, 0);padding:12px 35px 12px 35px;border-color:rgba(255, 255, 255, 0.50);border-style:solid;border-width:2px;letter-spacing:2px;">
                        EXPLORE EDANPAY
                    </div>
                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption rev-btn rev-bordered   rs-parallaxlevel-0" id="slide-214-layer-7"
                         data-x="['center','center','center','center']" data-hoffset="['180','180','0','0']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['120','70','50','50']"
                         data-width="none" data-height="none" data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:500;e:Linear.easeNone;"
                         data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(0, 0, 0, 1.00);bc:rgba(0, 0, 0, 1.00);"
                         data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:[-100%];s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2500"
                         data-splitin="none"
                         data-splitout="none"
                         data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"#contact","offset":"px"}]'
                         data-responsive_offset="off" data-responsive="off"
                         style="z-index: 11; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 600; color: rgba(255, 255, 255, 1.00);font-family:Raleway;background-color:rgba(219, 28, 34, 1.00);padding:12px 35px 12px 35px;border-color:rgba(219, 28, 34, 0);border-style:solid;border-width:2px;letter-spacing:2px;">
                        CONTACT NOW
                    </div>
                </li>
            </ul>
            <div class="tp-static-layers"></div>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
    <section>
        <div class="container">
            <div class="text-center">
                <h1 class="font-lato">Welcome to <span>Edan Pay</span></h1>
                <h2 class="col-sm-10 col-sm-offset-1 nomargin-bottom weight-400">
                    Edan Pay are committed to changing the way businesses and homes currently source their
                    payments services, via new pioneer solutions that also provide significant cost savings for
                    businesses.
                </h2>
            </div>
        </div>
    </section>
    @include('components.callouts.parallax-2')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <header class="margin-bottom-60">
                        <h2>With Edan Pay</h2>
                    </header>
                    <div class="toggle toggle-transparent toggle-bordered-full">
                        <div class="toggle active">
                            <label>Card Machines</label>
                            <div class="toggle-content">
                                <p>Mobile Card Machine, Portable Card Machine, Countertop Card Machine, Debit & Credit
                                    Card Machines, Wi-Fi & Portable, Contactless Card Machines,. Our specialists will
                                    find the
                                    most cost-effective and appropriate deals for you.</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>Debit & Credit Card Processing</label>
                            <div class="toggle-content">
                                <p>Card Processing is our speciality at Edan Pay. We are proud to offer a
                                    wide array of debit and credit card processing services – whether you are new to
                                    credit card processing or you are looking to switch your merchant account providers
                                    and save up to 50% on rates, our UK-based team of experts will be happy to help
                                    you on Affordable payment processing solutions.</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>Apple Pay &amp; Android Pay</label>
                            <div class="toggle-content">
                                <p>Apple Pay, Android Pay &amp; Samsung Pay are a revolutionary new service which is sure to change the way in which we finalise our purchases at the checkout.
                                    You can upgrade your non contactless terminal by calling us today</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>Online Payment Solutions</label>
                            <div class="toggle-content">
                                <p>To accept e-commerce payments online we  install software and online payment systems, giving you the choice and flexibility needed to build and sustain a profitable online business.
                                    E-commerce Payments, Sell via your Tablet, Sell via your Website using our best solutions for Payment Gateway.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <header class="margin-bottom-60">
                        <h2>Why Edan Pay?</h2>
                    </header>
                    <div class="toggle toggle-transparent toggle-bordered-full">
                        <div class="toggle active">
                            <label>Proven track record</label>
                            <div class="toggle-content">
                                <p>We’ve helped over 1,500 companies make savings across their business, with a 98%
                                    recommendation rate.</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>Dedicated customer experience team</label>
                            <div class="toggle-content">
                                <p> We will protect you every step of the way, ensuring you continue to receive the best
                                    prices when your contracts are up for renewal.</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>Tried and tested suppliers</label>
                            <div class="toggle-content">
                                <p>We only work with the very best. You, the customer, are what’s most important to us
                                    and saving you money is our number one priority. We make sure that we not only get
                                    you the cheapest deal, but also one that is tailor-made for your business.</p>
                            </div>
                        </div>
                        <div class="toggle">
                            <label>More services than anyone else</label>
                            <div class="toggle-content">
                                <p>We don’t just stop with your business insurance, we can make you savings on over 12
                                    different areas of your business, more than any of our competitors.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    @include('components.callouts.transparent-2')
    <section class="alternate-2" id="contact">
        @include('partners.contact-form')
    </section>

    <footer id="footer">
        <div class="copyright">
            <div class="container">
                <ul class="pull-right nomargin list-inline mobile-block">
                    <li><a href="#">Terms &amp; Conditions</a></li>
                    <li>&bull;</li>
                    <li><a href="#">Privacy</a></li>
                </ul>
                {{date('Y')}} &copy; All Rights Reserved, Edan Power Ltd.
            </div>
        </div>
    </footer>
@endsection
@section('script')
    <script type="text/javascript"
            src="{{asset('assets/plugins/slider.revolution.v5/js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('assets/plugins/slider.revolution.v5/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            if (jQuery("#rev_slider_56_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_56_1");
            } else {
                revapi56 = jQuery("#rev_slider_56_1").show().revolution({
                    sliderType: "hero",
                    jsFileLocation: plugin_path + "slider.revolution.v5/js/",
                    sliderLayout: "fullwidth",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {},
                    responsiveLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [720, 640, 640, 640],
                    lazyType: "none",
                    parallax: {
                        type: "scroll",
                        origo: "enterpoint",
                        speed: 400,
                        levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                    },
                    shadow: 0,
                    spinner: "off",
                    autoHeight: "off",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        disableFocusListener: false,
                    }
                });
            }
        });
        $(function () {
            $("input:file").change(function () {
                var files = $(this).prop("files");
                var names = $.map(files, function (val) {
                    return val.name;
                });
                var items = [];
                $.each(names, function (i, item) {
                    items.push('<li>' + item + '</li>');
                });
                $('.filename').html('').append(items.join(''));
            });
        });
    </script>
    @include('components.analytics',['id'=>'UA-104769260-3'])
@endsection
