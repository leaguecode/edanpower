<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
    @if(config('app.env') != 'production')
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
        <meta http-equiv="pragma" content="no-cache"/>
        @endif
    <!--[if IE]>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        @include('components.favicons')
        @yield('meta')
        @include('components.header-links')
        @yield('style')
</head>

<body class="smoothscroll enable-animation">

<div id="wrapper">
    @yield('content')
</div>
{{--SCROLL TO TOP--}}
<a href="#" id="toTop"></a>
{{--PRELOADER--}}
<div id="preloader">
    <div class="inner">
        <span class="loader"></span>
    </div>
</div>
@include('components.footer-links')
@include('vendor.sweet.alert')
@yield('script')
</body>
</html>