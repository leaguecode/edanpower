<section class="page-header page-header-xs">
    <div class="container">
        <h1>{{$title}}</h1>
        <!-- breadcrumbs -->
        <ol class="breadcrumb size-14">
            <li><a href="{{url('/')}}">Home</a></li>
            @if(isset($links))
                @foreach($links as $key => $link)
                    @if($loop->last)
                        <li class="active" >{{$key}}</li>
                    @else
                        <li ><a href="{{url($link)}}">{{$key}}</a></li>
                    @endif

                @endforeach
            @endif
        </ol>
        <!-- /breadcrumbs -->
    </div>
</section>