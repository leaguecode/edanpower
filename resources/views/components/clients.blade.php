<section>
    <div class="container">
        <div class="text-center">
            <div class="owl-carousel " data-plugin-options='{"singleItem": false, "items": "5","autoPlay": true}'>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/britishgas.png')}}" alt="Edan Power Partners - British Gas">
                </a>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/barclays.png')}}" alt="Edan Power Partners - Barclays">
                </a>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/d-energi.png')}}" alt="Edan Power Partners - D Energi">
                </a>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/engie.png')}}" alt="Edan Power Partners - Engie">
                </a>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/totalgp.png')}}" alt="Edan Power Partners - Total GP">
                </a>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/cng.png')}}" alt="Edan Power Partners - CNG">
                </a>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/e-on.png')}}" alt="Edan Power Partners - E-ON">
                </a>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/yu-energy.png')}}"  alt="Edan Power Partners - YU Energy">
                </a>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/gazprom.png')}}" alt="Edan Power Partners - Gazprom">
                </a>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/npower.png')}}" alt="Edan Power Partners - nPower">
                </a>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/opusenergy.png')}}" alt="Edan Power Partners - Opus Energy">
                </a>
                <a class="img-hover">
                    <img class="img-responsive" src="{{asset('assets/images/clients/havenpower.png')}}" alt="Edan Power Partners - Haven Power">
                </a>

            </div>
        </div>

    </div>
</section>