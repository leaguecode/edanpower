@if(isset($title))
    <title>{{$title}} - Edan Power </title>
    <meta property="twitter:title" content="{{$title}}"/>
    <meta property="og:title" content="{{$title}}"/>
@else
    <title>Edan Power - Powering Your Business</title>
    <meta property="twitter:title" content="Edan Power - Powering Your Business"/>
    <meta property="og:title" content="Edan Power - Powering Your Business"/>
@endif
<meta name="Keywords" content="{{ $keywords or env('META_KEYWORDS') }}">
@if(isset($description))
    <meta name="Description" content="{{$description}}  "/>
    <meta property="twitter:description" content="{{$description}}"/>
    <meta property="og:description" content="{{$description}}"/>
@else
    <meta name="Description" content="{{env('META_DESCRIPTION')}}"/>
    <meta property="twitter:description" content="{{env('META_DESCRIPTION')}}"/>
    <meta property="og:description" content="{{env('META_DESCRIPTION')}}"/>
@endif
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@publisher_handle">
<meta name="twitter:creator" content="@author_handle">
@if(isset($image))
    <meta property="twitter:image" content="{{$image}}"/>
    <meta property="og:image" content="{{$image}}"/>
@else
    <meta property="twitter:image" content="{{asset('assets/images/edanpower/logo.png')}}"/>
    <meta property="og:image" content="{{asset('assets/images/edanpower/logo.png')}}"/>
@endif
<meta property="og:type" content="article"/>
<meta property="og:url" content="{{Request::url()}}"/>
<meta property="og:site_name" content="edanpower.co.uk"/>
<meta property="fb:admins" content=""/>

{{ $slot }}