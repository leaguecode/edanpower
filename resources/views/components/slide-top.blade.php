<div id="slidetop">

    <div class="container">

        <div class="row">

            <div class="col-md-4">
                <h6><i class="icon-heart"></i>EDANPOWER</h6>
                <p>Edanpower are committed to changing the way businesses and homeowners currently source their energy and communication requirements, via new greener solutions that also provide significant cost savings for businesses and residential dwellings. </p>
            </div>

            <div class="col-md-4">
                <h6><i class="fa fa-check"></i> RECENTLY VISITED</h6>
                <ul class="list-unstyled">
                    <li><a href="https://edantalk.co.uk"><i class="fa fa-angle-right"></i> Edan Talk</a></li>
                    <li><a href="https://edanpay.co.uk"><i class="fa fa-angle-right"></i> Edan Pay </a></li>
                    <li><a href="https://edancover.co.uk"><i class="fa fa-angle-right"></i> Edan Cover</a></li>
                </ul>
            </div>

            <div class="col-md-4">
                <h6><i class="icon-envelope"></i> CONTACT INFO</h6>
                <ul class="list-unstyled">
                    <li><b>Address:</b> {{env('ADDRESS')}}</li>
                    <li><b>Phone:</b> <a href="tel:{{env('PHONE')}}">{{env('PHONE')}}</a></li>
                    <li><b>Email:</b> <a href="mailto:{{env('EMAIL')}}">{{env('EMAIL')}}</a></li>
                </ul>
            </div>

        </div>

    </div>

    <a class="slidetop-toggle" href="#"><!-- toggle button --></a>

</div>