@include('components.slide-top')
<div id="header"
        @if (Route::currentRouteName() == 'home')
        class="clearfix transparent sticky noborder disable-uppercase shadow-after-3"
        @else
        class="sticky clearfix "
        @endif
>
    <!-- TOP NAV -->
    <header id="topNav">
        <div class="container">

            <!-- Mobile Menu Button -->
            <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                <i class="fa fa-bars"></i>
            </button>

            <a class="logo pull-left" href="{{url('/')}}">
                <img data-pagespeed-no-defer src="{{asset('assets/images/edanpower/logo.png')}}" alt="Edan Power Logo"
                     class="{{Route::currentRouteName() == 'home' ? "home-logo" : ''}}"/>
            </a>
            <div class="navbar-collapse pull-right nav-main-collapse collapse">
                <nav class="nav-main">
                    <ul id="topMain" class="nav nav-pills nav-main">
                        <li><a href="{{route('home')}}">HOME</a></li>
                        <li><a href="{{route('about')}}">ABOUT</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#">
                                SERVICES
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('electric')}}">Electric</a></li>
                                <li><a href="{{route('gas')}}">Gas</a></li>
                                <li><a href="{{route('water')}}">Water</a></li>
                                <li><a href="{{route('chipAndPin')}}">Chip and Pin</a></li>
                                <li><a href="{{route('telecoms')}}">Telecoms</a></li>
                                <li><a href="{{route('waste')}}">Waste</a></li>
                                {{--<li><a href="{{route('businessRateReview')}}">Business Rate Review</a></li>--}}
                                <li><a target="_blank" href="http://www.energylinx.co.uk/e2c/edanpower/">Domestic Energy</a></li>
                                {{--<li class="divider"></li>--}}
                                {{--<li class="dropdown">--}}
                                    {{--<a class="dropdown-toggle" href="#">--}}
                                        {{--More--}}
                                    {{--</a>--}}
                                    {{--<ul class="dropdown-menu">--}}
                                        {{--<li><a href="{{route('solarPlus')}}">Solar Plus</a></li>--}}
                                        {{--<li><a href="{{route('solarMax')}}">Solar Max</a></li>--}}
                                        {{--<li><a href="{{route('commercialSolar')}}">Commercial Solar</a></li>--}}
                                        {{--<li><a href="{{route('optiPlus')}}">Otiplus</a></li>--}}
                                        {{--<li><a href="{{route('optiMax')}}">Optimax</a></li>--}}
                                        {{--<li><a href="{{route('fuelPoverty')}}">Fuel Poverty</a></li>--}}
                                        {{--<li><a href="{{route('commercialLed')}}">Commercial Led</a></li>--}}
                                        {{--<li><a href="{{route('commercialStorage')}}">Commercial Storage</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{route('chp')}}">CHP</a></li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                            </ul>
                        </li>
                        <li><a href="{{route('jobs.index')}}">CAREERS</a></li>
                        <li><a href="{{route('blog.index')}}">BLOG</a></li>
                        <li><a href="{{route('contact')}}">CONTACT</a></li>
                    </ul>

                </nav>
            </div>

        </div>
    </header>
    <!-- /Top Nav -->

</div>