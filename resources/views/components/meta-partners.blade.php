@if(isset($title))
    <title>{{$title}} </title>
    <meta property="twitter:title" content="{{$title}}"/>
    <meta property="og:title" content="{{$title}}"/>
@endif
<meta name="Keywords" content="{{ $keywords or '' }}">
@if(isset($description))
    <meta name="Description" content="{{$description}}  "/>
    <meta property="twitter:description" content="{{$description}}"/>
    <meta property="og:description" content="{{$description}}"/>
@endif
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@publisher_handle">
<meta name="twitter:creator" content="@author_handle">
@if(isset($image))
    <meta property="twitter:image" content="{{$image}}"/>
    <meta property="og:image" content="{{$image}}"/>
@endif
<meta property="og:type" content="article"/>
<meta property="og:url" content="{{Request::url()}}"/>
<meta property="og:site_name" content="{{Request::server("HTTP_HOST")}}"/>
<meta property="fb:admins" content=""/>

{{ $slot }}