<footer id="footer" class="footer-light">
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <img class="footer-logo" src="{{asset('assets/images/edanpower/logo.png')}}" alt="Edan Power Logo"
                     height="60px"/>

                <p>For all your home and business gas, electric, water and waste.</p>

                <address>
                    <ul class="list-unstyled">
                        <li class="footer-sprite address">
                            {{env('ADDRESS')}}
                        </li>
                        <li class="footer-sprite phone">
                            Phone: <a href="tel:{{env('PHONE')}}">{{env('PHONE')}}</a>
                        </li>
                        <li class="footer-sprite email">
                            Email: <a href="mailto:{{env('EMAIL')}}">{{env('EMAIL')}}</a>
                        </li>
                    </ul>
                </address>

            </div>
            <div class="col-md-3">

                <h4 class="letter-spacing-1">LATEST NEWS</h4>
                <ul class="footer-posts list-unstyled">
                    @foreach(\App\Blog::getTop(5) as $blog)
                        <li>
                            <a href="{{route('blog.show',['slug'=>str_slug($blog->title),'blog'=>$blog])}}">{!! $blog->title !!}</a>
                            <small>{{date('d M, Y',strtotime($blog->published_at))}}</small>
                        </li>
                    @endforeach
                </ul>

            </div>
            <div class="col-md-2">

                <h4 class="letter-spacing-1">EXPLORE</h4>
                <ul class="footer-links list-unstyled">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li><a href="{{route('about')}}">About Us</a></li>
                    <li><a>Our Services</a></li>
                    <li><a href="{{route('blog.index')}}">Our Blog</a></li>
                    <li><a href="{{route('jobs.index')}}">Career</a></li>
                    <li><a href="{{route('contact')}}">Contact Us</a></li>
                     <li><a href="https://cardmachines.edanpower.co.uk/">Edan Pay Card Machines</a></li>
                </ul>

            </div>
            <div class="col-md-4">

                <h4 class="letter-spacing-1">KEEP IN TOUCH</h4>
                <p>Subscribe to Our Newsletter to get Important News &amp; Offers</p>
                <form class="validate" action="" method="post">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="email" id="email" name="email" class="form-control required"
                               placeholder="Enter your Email">
                        <span class="input-group-btn">
										<button class="btn btn-success" type="submit">Subscribe</button>
									</span>
                    </div>
                </form>
                <!-- /Newsletter Form -->
                <!-- Social Icons -->
                <div class="margin-top-20">
                    <a href="{{env('FACEBOOK')}}" class="social-icon social-icon-border social-facebook pull-left" data-toggle="tooltip"
                       data-placement="top" title="Facebook">

                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                    </a>
                    <a href="{{env('TWITTER')}}" class="social-icon social-icon-border social-twitter pull-left" data-toggle="tooltip"
                       data-placement="top" title="Twitter">
                        <i class="icon-twitter"></i>
                        <i class="icon-twitter"></i>
                    </a>
                    <a href="{{env('LINKEDIN')}}" class="social-icon social-icon-border social-linkedin pull-left" data-toggle="tooltip"
                       data-placement="top" title="Linkedin">
                        <i class="icon-linkedin"></i>
                        <i class="icon-linkedin"></i>
                    </a>
                </div>
                <!-- /Social Icons -->
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <ul class="pull-right nomargin list-inline mobile-block">
                <li><a href="{{route('terms')}}">Terms &amp; Conditions</a></li>
                <li>&bull;</li>
                <li><a href="{{route('privacy')}}">Privacy</a></li>
            </ul>
            {{date('Y')}} &copy; All Rights Reserved, Edan Power Ltd.
        </div>
    </div>
</footer>