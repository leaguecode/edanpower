<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@publisher_handle">
<meta name="twitter:creator" content="@author_handle">

@if(isset($title))
    <meta property="twitter:title" content="{{$title}}"/>
@endif
@if(isset($description))
    <meta property="twitter:description" content="{{$description}}"/>
@endif
@if(isset($image))
    <meta property="twitter:image" content="{{$image}}"/>
@endif
{{ $slot }}