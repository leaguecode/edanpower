<div class="callout alert alert-success noborder margin-top-60 nomargin-bottom">

    <div class="text-center">

        <h3>Call now at <strong><a href="tel:{{env('PHONE')}}"><span>+{{env('PHONE')}}</span> </a></strong></h3>
        <p class="font-lato size-20">
            We truly care about our users and our product.
        </p>

        <a href="{{url('contact')}}" rel="nofollow" target="_blank" class="btn btn-success btn-lg margin-top-30">CONTACT NOW</a>

    </div>

</div>