        <div class="callout callout-theme-color">
            <div class="container">
                    <div class="text-center"><!-- title + shortdesc -->
                        <h1>Call now at <strong><a href="tel:{{env('PHONE')}}">+{{env('PHONE')}}</a></strong></h1>
                        <p> We truly care about our users and our product.</p>
                    </div>
            </div>
        </div>

