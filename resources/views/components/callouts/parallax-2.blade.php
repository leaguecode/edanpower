<section class="parallax parallax-2" style="background-image: url({{url('assets/images/misc/px-001.jpg')}})">
    <div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>

    <div class="container text-center">

        <h2 class="font-raleway margin-bottom-10">
            <span class="countTo" data-speed="4000">5899</span> clients
        </h2>

        <h3 class="size-25">They're <span>satisfied</span>. We're <span>happy</span>!</h3>

    </div>

</section>