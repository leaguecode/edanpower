<div class="alert alert-transparent bordered-bottom">
    <div class="container">

        <div class="row">

            <div class="col-md-8 col-sm-8">
                <h3>Call now at <a href="tel:{{env('PHONE')}}" class="btn-primary"><span><strong>+{{env('PHONE')}}</strong></span> </a></h3>
                    <p class="font-lato weight-300 size-20 nomargin-bottom">
                        We truly care about our users and our product.
                    </p>
            </div>


            <div class="col-md-4 col-sm-4 text-right">
                <a href="{{url('contact')}}" rel="nofollow" target="_blank" class="btn btn-primary btn-lg">CONTACT NOW</a>
            </div>

        </div>

    </div>
</div>