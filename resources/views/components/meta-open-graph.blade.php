@if(isset($title))
    <meta property="og:title" content="{{$title}}"/>
@endif
<meta property="og:type" content="article"/>
<meta property="og:url" content="{{Request::url()}}"/>
@if(isset($image))
    <meta property="og:image" content="{{$image}}"/>
@endif
@if(isset($description))
    <meta property="og:description" content="{{$description}}"/>
@endif
<meta property="og:site_name" content="edanpower.co.uk"/>
<meta property="fb:admins" content=""/>
{{ $slot }}



