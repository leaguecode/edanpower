<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'description',
        'keywords',
        'active',
        'published_at',
        'edited_at',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    public function media()
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getTop($limit = 5)
    {
        return Blog::where('active', true)->orderBy('published_at', 'DESC')->take($limit)->get();
    }
}
