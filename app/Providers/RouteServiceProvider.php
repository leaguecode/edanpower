<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        if (env('APP_ENV') === 'production') {
            \URL::forceScheme('https');
        }
        \Route::pattern('domain', '[a-z0-9.]+');
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapOfficeRoutes();
        $this->mapOtherEdanDomainRoutes();
        //
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->domain(env('DOMAIN_POWER'))
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    protected function mapOfficeRoutes()
    {
        Route::middleware('web')
            ->name('office.')
            ->domain('office.' . env('DOMAIN_POWER'))
            ->namespace($this->namespace)
            ->group(base_path('routes/office.php'));
    }

    protected function mapOtherEdanDomainRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/otherDomain.php'));
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->domain(env('DOMAIN_POWER'))
            ->group(base_path('routes/web.php'));
    }
}
