<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Contact extends Model
{
    use SoftDeletes, Notifiable;
    protected $fillable = [
        "name",
        "email",
        "phone",
        "company",
        "site",
        "ip",
        "other",
    ];

    public function media()
    {
        return $this->morphMany(Media::class, 'mediable');
    }
}
