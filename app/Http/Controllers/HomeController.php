<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    
    public function home()
    {
        return view('pages.home');
    }

    public function about()
    {
        return view('pages.about');
    }

    public function electric()
    {
        return view('pages.electric');
    }

    public function gas()
    {
        return view('pages.gas');
    }

    public function water()
    {
        return view('pages.water');
    }

    public function chipAndPin() {
        return view('pages.chip-and-pin');
    }
//
//    public function fuelPoverty()
//    {
//        return view('pages.fuel-poverty');
//    }
//
    public function telecoms() {
        return view('pages.telecoms');
    }

    public function waste()
    {
        return view('pages.waste');
    }
//
//    public function businessRateReview()
//    {
//        return view('pages.business-rate-review');
//    }
//
//    public function solarPlus() {
//        return view('pages.solar-plus');
//    }
//
//    public function solarMax() {
//        return view('pages.solar-max');
//    }
//
//    public function optiPlus() {
//        return view('pages.opti-plus');
//    }
//
//    public function optimax() {
//        return view('pages.opti-max');
//    }
//
//    public function commercialSolar() {
//        return view('pages.commercial-solar');
//    }
//
//    public function commercialLed() {
//        return view('pages.commercial-led');
//    }
//
//    public function commercialStorage() {
//        return view('pages.commercial-storage');
//    }
//
//    public function chp() {
//        return view('pages.chp');
//    }

}
