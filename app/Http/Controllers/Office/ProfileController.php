<?php

namespace App\Http\Controllers\Office;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;
use Auth;use ImageOptimizer;
use App\Media;
class ProfileController extends Controller
{
    /**
     *
     */
    public function profileIndex()
    {
        return view('office.settings.profile');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profileUpdate(Request $request)
    {
        $this->validate($request,['name'=>'required']);
        $user = Auth::user();
        $user->name = $request->name;
        $user->about = $request->about;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->save();
        Alert::success('Changes are saved');
        return redirect()->back();
    }

    /**
     *
     */
    public function passwordIndex()
    {
        return view('office.settings.password');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function passwordUpdate(Request $request)
    {
        $this->validate($request,[ 'password' => 'required|string|min:6|confirmed',]);
        $user = Auth::user();
        $user->password = bcrypt($request->password);
        $user->save();
        Alert::success('Your password is changed');
        return redirect()->back();
    }

    /**
     *
     */
    public function avatarIndex()
    {
        return view('office.settings.avatar');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function avatarUpdate(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|image'
        ]);

        $path = $request->file('file')->store('public');
        $name = explode('/', $path)[1];
        $mime = \Storage::mimeType($path);
        if (substr($mime, 0, 5) == 'image') {
            ImageOptimizer::optimize(storage_path('app/' . $path));
        }
        $media = new Media();
        $media->name = $name;
        $media->mime = $mime;
        $user = Auth::user();
        $user->media()->save($media);
        $user->avatar = $name;
        $user->save();

        return redirect()->back();
    }

    /**
     * @param $name
     * @return \Illuminate\Http\RedirectResponse
     */
    public function avatarDelete($name)
    {
        $media = Media::where('name',$name)->firstOrFail();
        \Storage::delete('public/'.$media->name);
        $media->delete();
        Auth::user()->avatar = '';
        Auth::user()->save();
        return redirect()->action('Office\ProfileController@avatarIndex');
    }
}
