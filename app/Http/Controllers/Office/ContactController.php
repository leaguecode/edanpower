<?php

namespace App\Http\Controllers\Office;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::orderBy('created_at','DESC')->paginate(25);
        return view('office.contacts.index', compact('contacts'));
    }

    public function show(Contact $contact)
    {
        return view('office.contacts.show',compact('contact'));
    }

    public function bySite($site)
    {
        $contacts = Contact::where('site','like',$site.'%')->orderBy('created_at','DESC')->paginate(25);
//        return $contacts;
        return view('office.contacts.index', compact('contacts'));
    }


}
