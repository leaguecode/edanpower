<?php

namespace App\Http\Controllers\Office;

use App\Job;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Media;
use Carbon\Carbon;
use Illuminate\Support\Str;
use ImageOptimizer;
class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::orderBy('created_at','DESC')->paginate(25);
        return view('office.jobs.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.jobs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
        $job = new Job;
        $job->title = $request->title;
        $job->user_id = Auth::user()->id;
        $job->save();
        return redirect()->action('Office\JobController@edit', ['job' => $job]);
    }


    /**
     * @param Job $job
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Job $post
     */
    public function show(Job $job)
    {
        return view('office.jobs.show', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Job $job
     * @return \Illuminate\Http\Response
     * @internal param Job $post
     * @internal param int $id
     */
    public function edit(Job $job)
    {
        return view('office.jobs.edit', compact('job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Job $job
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Job $job)
    {

        $this->validate($request, [
            'title' => 'required',
        ]);

        $job->title = $request->title;
        $job->description = $request->description;
        $job->requirements = $request->requirements;
        $job->expecting = $request->expecting;
        $job->vacancies = $request->vacancies;
        $job->ends_at = Carbon::parse($request->ends_at);
        $job->meta_title = isset($request->meta_title) ? $request->meta_title : $request->title;
        $job->meta_description = isset($request->meta_description) ? strip_tags($request->meta_description) : strip_tags(str_limit($request->description, $limit = 150));
        $job->meta_keywords = $request->meta_keywords;
        if (isset($request->file)) {
            $job->cover = $this->UploadFile($request, $job);
        }
        $job->user_id = Auth::user()->id;
        $job->edited_at = Carbon::now();
        $job->save();
        return redirect()->action('Office\JobController@index');
    }

    public function upload(Request $request, Job $job)
    {
        $name = $this->UploadFile($request, $job);
        return asset('storage/' . $name);

    }

    private function UploadFile(Request $request, Job $job)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);

        $path = $request->file('file')->store('public');
        $name = explode('/', $path)[1];
        $mime = \Storage::mimeType($path);
        if (substr($mime, 0, 5) == 'image') {
            ImageOptimizer::optimize(storage_path('app/' . $path));
        }
        $media = new Media();
        $media->name = $name;
        $media->mime = $mime;
        $job->media()->save($media);
        return $name;
    }

    public function active(Request $request, Job $job)
    {
        $this->validate($request, [
            'active' => 'required',
        ]);

        if ($request->active) {
            $job->active = $request->active;
            $job->published_at = Carbon::now();
        } else {
            $job->active = $request->active;
        }
        $job->save();
        return redirect()->action('Office\JobController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Job $job
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Job $job)
    {
        $job->media()->delete();
        $job->delete();
        return redirect()->action('Office\JobController@index');
    }

    public function preview(Job $job)
    {
        return view('office.jobs.preview', compact('job'));
    }
}
