<?php

namespace App\Http\Controllers\Office;

use App\Blog;
use App\Media;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use ImageOptimizer;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::orderBy('created_at','DESC')->paginate(25);
        return view('office.blog.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
        $blog = new Blog;
        $blog->title = $request->title;
        $blog->user_id = Auth::user()->id;
        $blog->save();
        return redirect()->action('Office\BlogController@edit', ['blog' => $blog]);
    }


    /**
     * @param Blog $blog
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Blog $post
     */
    public function show(Blog $blog)
    {
        return view('office.blog.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Blog $blog
     * @return \Illuminate\Http\Response
     * @internal param Blog $post
     * @internal param int $id
     */
    public function edit(Blog $blog)
    {
        return view('office.blog.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Blog $blog
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Blog $blog)
    {

        $this->validate($request, [
            'title' => 'required',
        ]);

        $blog->title = $request->title;
        $blog->description = $request->description;
        $blog->keywords = $request->keywords;
        $blog->meta_title = isset($request->meta_title) ? $request->meta_title : $request->title;
        $blog->meta_description = isset($request->meta_description) ? strip_tags($request->meta_description) : strip_tags(str_limit($request->description, $limit = 150));
        $blog->meta_keywords = isset($request->meta_keywords) ? $request->meta_keywords : $request->keywords;
        if (isset($request->file)) {
            $blog->cover = $this->UploadFile($request, $blog);
        }
        $blog->user_id = Auth::user()->id;
        $blog->edited_at = Carbon::now();
        $blog->save();
        return redirect()->action('Office\BlogController@show', [$blog]);
    }

    public function upload(Request $request, Blog $blog)
    {
        $name = $this->UploadFile($request, $blog);
        return asset('storage/' . $name);

    }

    private function UploadFile(Request $request, Blog $blog)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);
        $path = $request->file('file')->store('public');
        $name = explode('/', $path)[1];
        $mime = \Storage::mimeType($path);
        if (substr($mime, 0, 5) == 'image') {
            ImageOptimizer::optimize(storage_path('app/' . $path));
        }
        $media = new Media();
        $media->name = $name;
        $media->mime = $mime;
        $blog->media()->save($media);
        return $name;
    }

    public function active(Request $request, Blog $blog)
    {
        $this->validate($request, [
            'active' => 'required',
        ]);
        if ($request->active) {
            $blog->active = $request->active;
            $blog->published_at = Carbon::now();
        } else {
            $blog->active = $request->active;
        }
        $blog->save();
        return redirect()->action('Office\BlogController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Blog $blog
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Blog $blog)
    {
        $blog->media()->delete();
        $blog->delete();
        return redirect()->action('Office\BlogController@index');
    }

    public function preview(Blog $blog)
    {
        return view('office.blog.preview',compact('blog'));
    }
}
