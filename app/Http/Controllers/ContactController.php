<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactRequest;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Alert;
use Storage;
use ImageOptimizer;

class ContactController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = new Contact;
        $contact->name = $request->name;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
        $contact->company = $request->company;
        $contact->site = $request->site;
        $contact->ip = $request->ip();
        $contact->message = $request->message;
        $contact->save();
        if ($request->file('files')) {
            foreach ($request->file('files') as $file) {
                $path = $file->store('public');
                $name = explode('/', $path)[1];
                $mime = Storage::mimeType($path);
                $media = new Media();
                $media->name = $name;
                $media->mime = $mime;
                $contact->media()->save($media);
                if (substr($mime, 0, 5) == 'image') {
                    ImageOptimizer::optimize(storage_path('app/' . $path));
                }
            }
        }

        //email
        Alert::success('Thank you, You will be receiving a email shortly', 'Your Message Received')->persistent('Close');
        return redirect()->back();

    }

}
