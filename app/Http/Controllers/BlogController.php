<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::whereActive(true)->orderby('published_at','DESC')->paginate(5);
        return view('blog.index',compact('blogs'));
    }

    /**
     * Display the specified resource.
     *
     * @param $slug
     * @param Blog $blog
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show($slug , Blog $blog)
    {
        if (!$blog->active){
            abort('404');
        }
        return view('blog.show',compact('blog'));
    }


}
