<?php

namespace App\Http\Controllers;

use App\Job;
use App\Media;
use App\Seeker;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Alert;
use Storage;
use ImageOptimizer;
class JobController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::whereActive(true)->orderby('published_at', 'DESC')->paginate(5);
        return view('jobs.index', compact('jobs'));
    }


    /**
     * Display the specified resource.
     *
     * @param $slug
     * @param Job $job
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show($slug, Job $job)
    {
        if (!$job->active) {
            abort('404');
        }
        return view('jobs.show', compact('job'));
    }

    public function seeker(Request $request, Job $job)
    {
        $seeker = new Seeker();
        $seeker->name = $request->name;
        $seeker->email = $request->email;
        $seeker->phone = $request->phone;
        $seeker->description = $request->description;
        $seeker->job_id = $job->id;
        $seeker->save();
        if (isset($request->file)) {
            $this->UploadFile($request, $seeker);
        }
        Alert::success('Thank you, We\'ll get back to you within 48 hours', 'Your CV is Received')->persistent('Close');
        return redirect()->back();
    }

    private function UploadFile(Request $request, Seeker $seeker)
    {
        $path = $request->file('file')->store('public');
        $name = explode('/', $path)[1];
        $mime = Storage::mimeType($path);
        $media = new Media();
        $media->name = $name;
        $media->mime = $mime;
        $seeker->media()->save($media);
        if(substr($mime, 0, 5) == 'image') {
            ImageOptimizer::optimize(storage_path('app/'.$path));
        }
        return $name;
    }


}
