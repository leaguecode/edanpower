<?php

namespace App\Http\Controllers\Api;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;
use Storage;
use ImageOptimizer;
use App\Media;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $contact = new Contact;
        $contact->name = $request->name;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
        $contact->company = $request->company;
        $contact->site = $request->site;
        $contact->ip = $request->ip();
        $contact->message = $request->message;
        $contact->save();
        if ($request->file('files')) {
            foreach ($request->file('files') as $file) {
                $path = $file->store('public');
                $name = explode('/', $path)[1];
                $mime = Storage::mimeType($path);
                $media = new Media();
                $media->name = $name;
                $media->mime = $mime;
                $contact->media()->save($media);
                if (substr($mime, 0, 5) == 'image') {
                    ImageOptimizer::optimize(storage_path('app/' . $path));
                }
            }
        }
       
        return \Response::json(array('success' => true, 'data' => "success"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
