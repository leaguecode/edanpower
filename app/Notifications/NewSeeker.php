<?php

namespace App\Notifications;

use App\Job;
use App\Seeker;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewSeeker extends Notification
{
    use Queueable;
    public $job;
    public $seeker;

    /**
     * NewSeeker constructor.
     * @param $job
     * @param $seeker
     */
    public function __construct(Job $job,Seeker $seeker)
    {
        $this->job = $job;
        $this->seeker = $seeker;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
            ->subject('Edan Office: New Job Seeker for ' .$this->job->title )
            ->greeting('Hello.')
            ->line('You got a New Job Seeker for '. $this->job->title)
            ->line('Name : ' . $this->seeker->name)
            ->line('Phone : ' . $this->seeker->phone)
            ->line('Email : ' . $this->seeker->email)
            ->line('Thank you for using our Edan Office !');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
