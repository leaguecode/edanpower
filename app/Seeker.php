<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Seeker extends Model
{
    use Notifiable;
    protected $fillable = [
        'name',
        'email',
        'phone',
        'description',
        'job_id',
    ];
    public function media()
    {
        return $this->morphMany(Media::class, 'mediable');
    }
    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
