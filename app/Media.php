<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = ["name","mine","disk",];
    public function mediable()
    {
        return $this->morphTo();
    }
}
