<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Job extends Model
{
    use Notifiable;
    use SoftDeletes;
    protected $fillable = [
        "title",
        "description",
        "requirements",
        "expecting",
        "vacancies",
        "skills",
        "active",
        "meta_title",
        "meta_description",
        "meta_keywords",
        "ends_at",
        "published_at",
        "edited_at"
    ];
    public function media()
    {
        return $this->morphMany(Media::class, 'mediable');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function seekers()
    {
        return $this->hasMany(Seeker::class);
    }
}
