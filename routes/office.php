<?php
Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::group(['as' => 'dashboard.'], function () {
        Route::get('/', 'Office\DashboardController@index')->name('index');
    });
    Route::group(['as' => 'settings.','prefix'=>'settings/'], function () {
        Route::get('profile', 'Office\ProfileController@profileIndex')->name('profile.index');
        Route::put('profile', 'Office\ProfileController@profileUpdate')->name('profile.update');
        Route::get('password', 'Office\ProfileController@passwordIndex')->name('password.index');
        Route::put('password', 'Office\ProfileController@passwordUpdate')->name('password.update');
        Route::get('avatar', 'Office\ProfileController@avatarIndex')->name('avatar.index');
        Route::put('avatar', 'Office\ProfileController@avatarUpdate')->name('avatar.update');
        Route::get('avatar/remove/{name}', 'Office\ProfileController@avatarDelete')->name('avatar.delete');
    });
    //jobs
    Route::resource('jobs','Office\JobController');
    Route::post('jobs/upload/{job}/','Office\JobController@upload')->name('jobs.upload');
    Route::post('jobs/active/{job}/','Office\JobController@active')->name('jobs.active');
    Route::get('jobs/preview/{job}/','Office\JobController@preview')->name('jobs.preview');
    // Job Seekers
    Route::get('jobs/{job}/seekers','Office\SeekerController@index')->name('jobs.seekers.index');
    Route::get('jobs/{job}/seekers/{seeker}','Office\SeekerController@show')->name('jobs.seekers.show');
    //Blog
    Route::resource('blog','Office\BlogController');
    Route::post('blog/upload/{blog}/','Office\BlogController@upload')->name('blog.upload');
    Route::post('blog/active/{blog}/','Office\BlogController@active')->name('blog.active');
    Route::get('blog/preview/{blog}/','Office\BlogController@preview')->name('blog.preview');
    //Contacts
    Route::get('contacts/','Office\ContactController@index')->name('contacts.index');
    Route::get('contacts/{contact}','Office\ContactController@show')->name('contacts.show');
    Route::get('contacts/site/{site}','Office\ContactController@bySite')->name('contacts.site');
    //Users
    Route::resource('users','Office\UserController');
    //Permission
    Route::resource('permissions','Office\PermissionController');
    //Roles
    Route::resource('roles','Office\RoleController');
});