<?php

Route::domain(env('DOMAIN_COVER'))->group(function () {
    Route::view('/', 'partners.edancover.index');
    Route::post('/', 'ContactController@store');
});

Route::domain(env('DOMAIN_PAY'))->group(function () {
    Route::view('/', 'partners.edanpay.index');
    Route::post('/', 'ContactController@store');
});

Route::domain(env('DOMAIN_TALK'))->group(function () {
    Route::view('/', 'partners.edantalk.index');
    Route::post('/', 'ContactController@store');
});
