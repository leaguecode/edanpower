<?php
//Auth::routes();
Route::get('/', 'HomeController@home')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::group(['prefix' => 'services/', 'name' => 'services.'], function () {
    Route::get('/electric', 'HomeController@electric')->name('electric');
    Route::get('/gas', 'HomeController@gas')->name('gas');
    Route::get('/water', 'HomeController@water')->name('water');
    Route::get('/fuel-poverty', 'HomeController@fuelPoverty')->name('fuelPoverty');
    Route::get('/telecoms', 'HomeController@telecoms')->name('telecoms');
    Route::get('/chip-and-pin', 'HomeController@chipAndPin')->name('chipAndPin');
    Route::get('/waste', 'HomeController@waste')->name('waste');
    Route::get('/business-rate-review', 'HomeController@businessRateReview')->name('businessRateReview');
    //More
//    Route::get('/solar-plus', 'HomeController@solarPlus')->name('solarPlus');
//    Route::get('/solar-max', 'HomeController@solarMax')->name('solarMax');
//    Route::get('/opti-plus', 'HomeController@optiPlus')->name('optiPlus');
//    Route::get('/opti-max', 'HomeController@optiMax')->name('optiMax');
//    Route::get('/commercial-solar', 'HomeController@commercialSolar')->name('commercialSolar');
//    Route::get('/commercial-led', 'HomeController@commercialLed')->name('commercialLed');
//    Route::get('/commercial-storage', 'HomeController@commercialStorage')->name('commercialStorage');
//    Route::get('/chp', 'HomeController@chp')->name('chp');
});
//Blog
Route::group(['as' => 'blog.'], function () {
    Route::get('blog', 'BlogController@index')->name('index');
    Route::get('blog/{slug?}/{blog}', 'BlogController@show')->name('show');
});
//Career
Route::group(['as' => 'jobs.'], function () {
    Route::get('careers', 'JobController@index')->name('index');
    Route::get('careers/{slug?}/{job}', 'JobController@show')->name('show');
    Route::post('careers/{job}', 'JobController@seeker')->name('seeker');
});
//Contact
Route::get('contact', 'ContactController@index')->name('contact');
Route::post('contact', 'ContactController@store')->name('contact.store');

Route::get('error/{error}', function ($error) {
    return view('errors.' . $error);
});
Route::view('terms','pages.terms')->name('terms');
Route::view('privacy','pages.privacy')->name('privacy');
